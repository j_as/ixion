#version 120

varying vec4 color;
varying vec4 vertex;
varying vec4 normal;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;

uniform vec4 lightSource;

varying vec4 lightPosition;

void main() {
    color = gl_Color;
	lightPosition = viewMatrix * lightSource;
	
	mat4 modelViewMatrix = viewMatrix * modelMatrix;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    
    vertex = modelViewMatrix * gl_Vertex;
    normal = normalMatrix * vec4(gl_Normal, 1.0);
    gl_Position = modelViewProjectionMatrix * gl_Vertex;	
}