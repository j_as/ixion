#version 120

varying vec4 color;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

uniform sampler2D tex;

void main() {
	mat4 modelViewProjectionMatrix = projectionMatrix * viewMatrix * modelMatrix;
    color = gl_Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_Position = modelViewProjectionMatrix * gl_Vertex;
}