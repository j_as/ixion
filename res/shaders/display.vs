#version 120

varying float dist;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform sampler2D tex;

void main() {
	mat4 modelView = viewMatrix * modelMatrix;
    mat4 modelViewProjection = projectionMatrix * modelView;
	
    gl_TexCoord[0] = gl_MultiTexCoord0;
	    
    gl_Position = modelViewProjection * gl_Vertex;	
    dist = gl_Position.z;
}