#version 120

varying vec4 color;
varying vec4 vertex;
varying vec4 normal;

varying vec4 lightPosition;

uniform vec4 lightDiffuse;
uniform vec4 lightAmbient;
uniform float quadraticAttenuation;

void main() {
	vec4 N = normalize(normal);
    vec4 L = normalize(lightPosition - vertex);
    vec4 V = normalize(vertex);
	
	float NdotL = max(dot(N, L), 0.0);
	float d = length(lightPosition - vertex);
    float att;
    
    if (d > 0.0) {
    	att = 1.0 / (quadraticAttenuation * d * d + 1.0);
    }
    vec4 ambient = lightAmbient * color;
    vec4 diffuse = NdotL * att * lightDiffuse;
    
    gl_FragColor = vec4((diffuse + ambient).xyz, 1.0);
}