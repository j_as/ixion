#version 120

varying vec4 color;

uniform sampler2D tex;

void main() {
    gl_FragColor = color * texture2D(tex, gl_TexCoord[0].xy);
}