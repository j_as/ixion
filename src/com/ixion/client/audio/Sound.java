package com.ixion.client.audio;

import static org.lwjgl.openal.AL10.*;

import java.nio.FloatBuffer;
import java.util.*;

import org.lwjgl.BufferUtils;

import com.ixion.client.GLObject;

import vecmath.Vec3;

public class Sound implements GLObject {
	private static final List<Sound> sounds = new ArrayList<Sound>();

	public final static void create() {
		for (Sound sound : sounds) {
			sound.compile();
		}
	}

	public final static void destroy() {
		for (Sound sound : sounds) {
			sound.dispose();
		}
	}

	private String path;
	protected int buffer;
	protected int source;

	private FloatBuffer sourcePosition;
	private FloatBuffer sourceVelocity;
	private FloatBuffer listenerPosition;
	private FloatBuffer listenerVelocity;
	private FloatBuffer listenerOrientation;

	protected Sound(String filename) {
		path = "sound/" + filename;
		sounds.add(this);
	}

	public void compile() {
		buffer = alGenBuffers();
		source = alGenSources();

		int result;

		if ((result = alGetError()) != AL_NO_ERROR) {
			System.err.println("ERROR!" + getALErrorString(result));
		}

		loadSound(path);

		if ((result = alGetError()) != AL_NO_ERROR) {
			System.err.println("ERROR!" + getALErrorString(result));
			getALErrorString(result);
		}

		alSourcei(source, AL_BUFFER, buffer);

		if ((result = alGetError()) != AL_NO_ERROR) {
			System.err.println("ERROR!" + getALErrorString(result));
		}

		setSourcePosition(new Vec3(0, 0, 0));
		setListenerVelocity(new Vec3(1, 1, 1));
		setSourceVelocity(new Vec3(1, 1, 1));
		setLoopEnabled(false);
		setPitch(1.0f);
		setVolume(1.0f);
	}

	private void loadSound(String filename) {
		WaveData data = WaveData.create("sound/" + filename);
		if (data == null) {
			System.err.print("The sound file is not there!");
			return;
		}

		alBufferData(buffer, data.format, data.data, data.samplerate);
		data.dispose();
	}

	public void setSourcePosition(float x, float y, float z) {
		if (sourcePosition == null) {
			sourcePosition = BufferUtils.createFloatBuffer(3);
		}

		sourcePosition.clear();

		sourcePosition.put(x);
		sourcePosition.put(y);
		sourcePosition.put(z);

		sourcePosition.flip();

		alSource(source, AL_POSITION, sourcePosition);
	}

	public void setSourcePosition(Vec3 pos) {
		setSourcePosition(pos.x, pos.y, pos.z);
	}

	public void setSourceVelocity(float x, float y, float z) {
		if (sourceVelocity == null) {
			sourceVelocity = BufferUtils.createFloatBuffer(3);
		}

		sourceVelocity.clear();

		sourceVelocity.put(x);
		sourceVelocity.put(y);
		sourceVelocity.put(z);

		sourceVelocity.flip();

		alSource(source, AL_VELOCITY, sourceVelocity);
	}

	public void setSourceVelocity(Vec3 velocity) {
		setSourceVelocity(velocity.x, velocity.y, velocity.z);
	}

	public void setListenerPosition(float x, float y, float z) {
		if (listenerPosition == null) {
			listenerPosition = BufferUtils.createFloatBuffer(3);
		}

		listenerPosition.clear();

		listenerPosition.put(x);
		listenerPosition.put(y);
		listenerPosition.put(z);

		listenerPosition.flip();

		alListener(AL_POSITION, listenerPosition);
	}

	public void setListenerPosition(Vec3 position) {
		setListenerPosition(position.x, position.y, position.z);
	}

	public void setListenerVelocity(float x, float y, float z) {
		if (listenerVelocity == null) {
			listenerVelocity = BufferUtils.createFloatBuffer(3);
		}

		listenerVelocity.clear();

		listenerVelocity.put(x);
		listenerVelocity.put(y);
		listenerVelocity.put(z);

		listenerVelocity.flip();

		alListener(AL_VELOCITY, listenerVelocity);
	}

	public void setListenerVelocity(Vec3 velocity) {
		setListenerVelocity(velocity.x, velocity.y, velocity.z);
	}

	public void setListenerOrientation(float forwardx, float forwardy, float forwardz, float upx, float upy, float upz) {
		if (listenerOrientation == null) {
			listenerOrientation = BufferUtils.createFloatBuffer(3);
		}

		listenerOrientation.clear();

		listenerOrientation.put(forwardx);
		listenerOrientation.put(forwardy);
		listenerOrientation.put(forwardz);

		listenerOrientation.put(upx);
		listenerOrientation.put(upy);
		listenerOrientation.put(upz);

		listenerOrientation.flip();

		alListener(AL_ORIENTATION, listenerOrientation);
	}

	public boolean isPlaying() {
		return alGetSourcei(source, AL_PLAYING) == AL_TRUE;
	}

	public boolean isLooping() {
		return alGetSourcei(source, AL_LOOPING) == AL_TRUE;
	}

	public void setVolume(float volume) {
		alSourcef(source, AL_GAIN, volume);
	}

	public void setPitch(float pitch) {
		alSourcef(source, AL_PITCH, pitch);
	}

	public void setLoopEnabled(boolean enabled) {
		alSourcei(source, AL_LOOPING, enabled ? AL_TRUE : AL_FALSE);
	}

	public void play() {
		alSourcePlay(source);
	}

	public void pause() {
		alSourcePause(source);
	}

	public void stop() {
		alSourceStop(source);
	}

	public void rewind() {
		alSourceRewind(source);
	}

	private String getALErrorString(int error) {
		if (error == AL_NO_ERROR) return "AL_NO_ERROR";
		if (error == AL_INVALID_NAME) return "AL_INVALID_NAME";
		if (error == AL_INVALID_ENUM) return "AL_INVALID_ENUM";
		if (error == AL_INVALID_VALUE) return "AL_INVALID_VALUE";
		if (error == AL_INVALID_OPERATION) return "AL_INVALID_OPERATION";
		if (error == AL_OUT_OF_MEMORY) return "AL_OUT_OF_MEMORY";
		return "No such error code.";
	}

	public void dispose() {
		stop();

		alSourcei(source, AL_BUFFER, AL_NONE);
		alDeleteBuffers(buffer);
		alDeleteSources(source);
	}
}