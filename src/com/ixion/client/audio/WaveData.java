package com.ixion.client.audio;

import java.io.*;
import java.net.*;
import java.nio.*;

import javax.sound.sampled.*;

import static org.lwjgl.openal.AL10.*;

import com.ixion.client.*;
import com.sun.media.sound.WaveFileReader;

public class WaveData {
	public final ByteBuffer data;
	public final int format;
	public final int samplerate;

	private WaveData(ByteBuffer data, int format, int samplerate) {
		this.data = data;
		this.format = format;
		this.samplerate = samplerate;
	}

	public static WaveData create(URL path) {
		try {
			WaveFileReader wfr = new WaveFileReader();
			return create(wfr.getAudioInputStream(new BufferedInputStream(path.openStream())));
		} catch (Exception e) {
			org.lwjgl.LWJGLUtil.log("Unable to create from: " + path + ", " + e.getMessage());
			return null;
		}
	}

	public static WaveData create(String path) {
		return create(Thread.currentThread().getContextClassLoader().getResource(path));
	}

	public static WaveData create(InputStream is) {
		try {
			return create(AudioSystem.getAudioInputStream(is));
		} catch (Exception e) {
			org.lwjgl.LWJGLUtil.log("Unable to create from inputstream, " + e.getMessage());
			return null;
		}
	}

	public static WaveData create(byte[] buffer) {
		try {
			return create(AudioSystem.getAudioInputStream(new BufferedInputStream(new ByteArrayInputStream(buffer))));
		} catch (Exception e) {
			org.lwjgl.LWJGLUtil.log("Unable to create from byte array, " + e.getMessage());
			return null;
		}
	}

	public static WaveData create(ByteBuffer buffer) {
		try {
			byte[] bytes = null;

			if (buffer.hasArray()) {
				bytes = buffer.array();
			} else {
				bytes = new byte[buffer.capacity()];
				buffer.get(bytes);
			}
			return create(bytes);
		} catch (Exception e) {
			org.lwjgl.LWJGLUtil.log("Unable to create from ByteBuffer, " + e.getMessage());
			return null;
		}
	}

	public static WaveData create(AudioInputStream ais) {
		AudioFormat audioformat = ais.getFormat();

		int channels = 0;
		if (audioformat.getChannels() == 1) {
			if (audioformat.getSampleSizeInBits() == 8) {
				channels = AL_FORMAT_MONO8;
			} else if (audioformat.getSampleSizeInBits() == 16) {
				channels = AL_FORMAT_MONO16;
			} else {
				assert false : "Illegal sample size";
			}
		} else if (audioformat.getChannels() == 2) {
			if (audioformat.getSampleSizeInBits() == 8) {
				channels = AL_FORMAT_STEREO8;
			} else if (audioformat.getSampleSizeInBits() == 16) {
				channels = AL_FORMAT_STEREO16;
			} else {
				assert false : "Illegal sample size";
			}
		} else {
			assert false : "Only mono or stereo is supported";
		}

		ByteBuffer buffer = null;
		try {
			int available = ais.available();
			if (available <= 0) {
				available = ais.getFormat().getChannels() * (int) ais.getFrameLength() * ais.getFormat().getSampleSizeInBits() / 8;
			}
			byte[] buf = new byte[ais.available()];
			int read = 0, total = 0;
			while ((read = ais.read(buf, total, buf.length - total)) != -1 && total < buf.length) {
				total += read;
			}
			buffer = convertAudioBytes(buf, audioformat.getSampleSizeInBits() == 16, audioformat.isBigEndian() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
		} catch (IOException ioe) {
			return null;
		}

		WaveData wavedata = new WaveData(buffer, channels, (int) audioformat.getSampleRate());

		try {
			ais.close();
		} catch (IOException ioe) {
		}

		return wavedata;
	}

	private static ByteBuffer convertAudioBytes(byte[] audio_bytes, boolean two_bytes_data, ByteOrder order) {
		ByteBuffer dest = MemoryHandler.createByteBuffer(audio_bytes.length);
		ByteBuffer src = ByteBuffer.wrap(audio_bytes);
		src.order(order);
		if (two_bytes_data) {
			ShortBuffer dest_short = dest.asShortBuffer();
			ShortBuffer src_short = src.asShortBuffer();
			while (src_short.hasRemaining())
				dest_short.put(src_short.get());
		} else {
			while (src.hasRemaining())
				dest.put(src.get());
		}
		dest.rewind();
		return dest;
	}

	public void dispose() {
		data.clear();
	}
}
