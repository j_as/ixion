package com.ixion.client.renderer;

import java.awt.*;
import java.awt.image.*;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;

public class TextRenderer {
	private class CharacterObject {
		public int x, y;
		public int w, h;
	}

	private CharacterObject[] charArray = new CharacterObject[256];

	private Font font;
	private boolean antiAlias;
	private int fontSize = 0;
	private int fontHeight = 0;
	private int textureWidth = 1024;
	private int textureHeight = 1024;
	private int fontTexture = 0;

	public TextRenderer(Font font, boolean antiAlias) {
		this.font = font;
		this.fontSize = font.getSize() + 3;
		this.antiAlias = antiAlias;

		createSet();
		fontHeight -= 1;
		if (fontHeight <= 0) fontHeight = 1;
	}

	private BufferedImage getFontImage(char ch) {
		BufferedImage tempfontImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) tempfontImage.getGraphics();
		if (antiAlias == true) g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setFont(font);

		FontMetrics fontMetrics = g.getFontMetrics();
		int charWidth = fontMetrics.charWidth(ch) + 8;
		int charHeight = fontMetrics.getHeight() + 3;
		if (charWidth <= 0) charWidth = 7;
		if (charHeight <= 0) charHeight = fontSize;

		BufferedImage fontImage = new BufferedImage(charWidth, charHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gt = (Graphics2D) fontImage.getGraphics();
		if (antiAlias == true) gt.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		gt.setFont(font);
		gt.setColor(Color.WHITE);
		gt.drawString(String.valueOf(ch), 3, 1 + fontMetrics.getAscent());
		gt.dispose();
		return fontImage;
	}

	private void createSet() {
		BufferedImage imgTemp = new BufferedImage(textureWidth, textureHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) imgTemp.getGraphics();

		g.setColor(new Color(0, 0, 0, 1));
		g.fillRect(0, 0, textureWidth, textureHeight);

		int rowHeight = 0;
		int positionX = 0;
		int positionY = 0;

		for (int i = 0; i < 256; i++) {
			char ch = (char) i;

			BufferedImage fontImage = getFontImage(ch);
			CharacterObject newIntObject = new CharacterObject();

			newIntObject.w = fontImage.getWidth();
			newIntObject.h = fontImage.getHeight();

			if (positionX + newIntObject.w >= textureWidth) {
				positionX = 0;
				positionY += rowHeight;
				rowHeight = 0;
			}

			newIntObject.x = positionX;
			newIntObject.y = positionY;

			if (newIntObject.h > fontHeight) {
				fontHeight = newIntObject.h;
			}

			if (newIntObject.h > rowHeight) {
				rowHeight = newIntObject.h;
			}

			g.drawImage(fontImage, positionX, positionY, null);
			positionX += newIntObject.w;

			if (i < 256) charArray[i] = newIntObject;
			fontImage = null;
		}
		g.dispose();
		createTexture(imgTemp);
	}

	private void createTexture(BufferedImage imgTemp) {
		int pixels[] = new int[textureWidth * textureHeight];
		imgTemp.getRGB(0, 0, textureWidth, textureHeight, pixels, 0, textureWidth);

		ByteBuffer buffer = Texture.createByteBuffer(imgTemp);

		fontTexture = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, fontTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureWidth, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	private void drawQuad(Tessellator t, int color, float x0, float y0, float x1, float y1, float sx0, float sy0, float sx1, float sy1) {
		float w = x1 - x0;
		float h = y1 - y0;
		float tx = sx0 / textureWidth;
		float ty = sy0 / textureHeight;
		float sw = sx1 - sx0;
		float sh = sy1 - sy0;
		float rw = (sw / textureWidth);
		float rh = (sh / textureHeight);

		t.color(color);
		t.tex(tx, ty);
		t.vertex(x0, y0);
		t.tex(tx, ty + rh);
		t.vertex(x0, y0 + h);
		t.tex(tx + rw, ty + rh);
		t.vertex(x0 + w, y0 + h);
		t.tex(tx + rw, ty);
		t.vertex(x0 + w, y0);
	}

	public int getWidth(String msg) {
		int totalwidth = 0;
		CharacterObject intObject = null;
		int currentChar = 0;
		for (int i = 0; i < msg.length(); i++) {
			currentChar = msg.charAt(i);
			if (currentChar < 256) {
				intObject = charArray[currentChar];
			}

			if (intObject != null) totalwidth += intObject.w;
		}
		return totalwidth;
	}

	public float getHeight() {
		return fontHeight;
	}

	public void render(String msg, int color, float x, float y, int startIndex, int endIndex, float scaleX, float scaleY) {
		CharacterObject obj = null;
		int charCurrent;

		int totalwidth = 0;
		int i = startIndex;
		int d = 1;
		int c = 8;
		float startY = 0;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, fontTexture);

		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);

		while (i >= startIndex && i <= endIndex) {
			charCurrent = msg.charAt(i);
			if (charCurrent < 256) {
				obj = charArray[charCurrent];
			}

			if (obj != null) {
				if (d < 0) totalwidth += (obj.w - c) * d;
				if (charCurrent == '\n') {
					startY -= fontHeight * d;
					totalwidth = 0;
				} else {
					drawQuad(t, color, (totalwidth + obj.w) * scaleX + x, startY * scaleY + y, totalwidth * scaleX + x, (startY + obj.h) * scaleY + y, obj.x + obj.w, obj.y + obj.h, obj.x, obj.y);
					if (d > 0) totalwidth += (obj.w - c) * d;
				}
				i += d;
			}
		}
		t.end();
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);

		glDisable(GL_BLEND);
	}

	public static boolean isFontSupported(String fontname) {
		Font font[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		for (int i = 0; i < font.length; i++) {
			if (font[i].getName().equalsIgnoreCase(fontname)) return true;
		}
		return false;
	}
}