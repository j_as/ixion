package com.ixion.client.renderer;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;

public class Renderer {
	public TextRenderer textRenderer;
	public Tessellator tess = Tessellator.instance;

	public void init() {
		String fontName = "Helvetica";

		if (!TextRenderer.isFontSupported(fontName)) {
			System.err.println(fontName + " isn't supported on this machine.");
			fontName = "Tahoma";
		}
		textRenderer = new TextRenderer(new Font(fontName, Font.PLAIN, 40), true);
	}

	public void quad(float x, float y, float xs, float ys, int color) {
		tess.begin(GL_QUADS);
		tess.color(color);
		tess.quad(x, y, 0, x + xs, y, 0, x + xs, y + ys, 0, x, y + ys, 0);
		tess.end();
	}

	public void transparentQuad(float x, float y, float xs, float ys, int color, int alpha) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		tess.begin(GL_QUADS);
		tess.color(color, alpha);
		tess.quad(x, y, 0, x + xs, y, 0, x + xs, y + ys, 0, x, y + ys, 0);
		tess.end();

		glDisable(GL_BLEND);
	}

	public void texturedQuad(Texture tex, float x, float y) {
		tex.enable();

		tess.begin(GL_QUADS);
		tess.color(0xffffff);
		tess.tex(0, 1);
		tess.vertex(x, y);
		tess.tex(1, 1);
		tess.vertex(x + tex.w, y);
		tess.tex(1, 0);
		tess.vertex(x + tex.w, y + tex.h);
		tess.tex(0, 0);
		tess.vertex(x, y + tex.h);
		tess.end();

		tex.disable();
	}

	public void texturedTransparentQuad(Texture tex, float x, float y, int color, int alpha) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		tex.enable();

		tess.begin(GL_QUADS);
		tess.color(color, alpha);
		tess.tex(0, 1);
		tess.vertex(x, y);
		tess.tex(1, 1);
		tess.vertex(x + tex.w, y);
		tess.tex(1, 0);
		tess.vertex(x + tex.w, y + tex.h);
		tess.tex(0, 0);
		tess.vertex(x, y + tex.h);
		tess.end();

		tex.disable();
		glDisable(GL_BLEND);
	}

	public void renderString(String msg, float x, float y) {
		textRenderer.render(msg, 0xffffffff, x, y, 0, msg.length() - 1, 1, 1);
	}

	public void renderString(String msg, int color, float x, float y) {
		textRenderer.render(msg, color, x, y, 0, msg.length() - 1, 1, 1);
	}

	public void renderString(String msg, float x, float y, float scaleX, float scaleY) {
		textRenderer.render(msg, 0xffffffff, x, y, 0, msg.length() - 1, scaleX, scaleY);
	}

	public void renderString(String msg, int color, float x, float y, float scaleX, float scaleY) {
		textRenderer.render(msg, color, x, y, 0, msg.length() - 1, scaleX, scaleY);
	}

	public void renderHudText(String main, String sub, float x, float y) {
		sub = sub.toUpperCase();

		renderString(main, 0xffffffff, x, y + 32, 0.4f, 0.4f);
		renderString(sub, 0xffffffff, x, y, 0.6f, 0.6f);
	}

}