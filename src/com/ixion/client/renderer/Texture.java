package com.ixion.client.renderer;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.*;
import java.io.*;
import java.nio.*;
import java.util.*;

import javax.imageio.*;

import com.ixion.client.GLObject;
import com.ixion.client.MemoryHandler;

import computer.VirtualMonitor;

public class Texture implements GLObject {
	private static final List<Texture> textures = new ArrayList<Texture>();

	public final static void create() {
		for (Texture tex : textures) {
			tex.compile();
		}
	}

	public final static void destroy() {
		for (Texture tex : textures) {
			tex.dispose();
		}
	}

	public int w, h;
	private int textureID;
	private BufferedImage image;

	public static final Texture ixionLogoTexture = new Texture(loadBufferedImage("/textures/logo.png"));
	public static final Texture ixionLogoTextureSmall = new Texture(loadBufferedImage("/textures/logo-small.png"));
	public static final Texture monitorTexture = new Texture(new BufferedImage(VirtualMonitor.WIDTH_PIXELS, VirtualMonitor.HEIGHT_PIXELS, BufferedImage.TYPE_INT_ARGB));

	public Texture(BufferedImage image) {
		w = image.getWidth();
		h = image.getHeight();
		this.image = image;
		textures.add(this);
	}

	public void enable() {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	public void disable() {
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
	}

	public void replacePixels(int[] pixels) {
		byte pixels1[] = new byte[w * h * 4];
		ByteBuffer buffer = MemoryHandler.createByteBuffer(w * h * 4);

		for (int i = 0; i < pixels.length; i++) {
			int a = (pixels[i] >> 24) & 0xff;
			int r = (pixels[i] >> 16) & 0xff;
			int g = (pixels[i] >> 8) & 0xff;
			int b = (pixels[i] & 0xff);

			pixels1[(i * 4) + 0] = (byte) r;
			pixels1[(i * 4) + 1] = (byte) g;
			pixels1[(i * 4) + 2] = (byte) b;
			pixels1[(i * 4) + 3] = (byte) a;
		}

		buffer.clear();
		buffer.put(pixels1);
		buffer.position(0).limit(pixels1.length);
		buffer.rewind();

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	public void compile() {
		int pixels[] = new int[w * h];
		image.getRGB(0, 0, w, h, pixels, 0, w);

		ByteBuffer buffer = createByteBuffer(image);

		textureID = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST/*GL_LINEAR*/);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	public void dispose() {
		glDeleteTextures(textureID);
	}

	public static ByteBuffer createByteBuffer(BufferedImage image) {
		int w = image.getWidth();
		int h = image.getHeight();

		int pixels[] = new int[w * h];
		image.getRGB(0, 0, w, h, pixels, 0, w);
		byte pixels1[] = new byte[w * h * 4];
		ByteBuffer buffer = MemoryHandler.createByteBuffer(w * h * 4);

		for (int i = 0; i < pixels.length; i++) {
			int a = (pixels[i] >> 24) & 0xff;
			int r = (pixels[i] >> 16) & 0xff;
			int g = (pixels[i] >> 8) & 0xff;
			int b = (pixels[i] & 0xff);

			pixels1[(i * 4) + 0] = (byte) r;
			pixels1[(i * 4) + 1] = (byte) g;
			pixels1[(i * 4) + 2] = (byte) b;
			pixels1[(i * 4) + 3] = (byte) a;
		}

		buffer.clear();
		buffer.put(pixels1);
		buffer.position(0).limit(pixels1.length);
		buffer.rewind();
		return buffer;
	}

	public static BufferedImage loadBufferedImage(String path) {
		try {
			return ImageIO.read(Texture.class.getResource(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}