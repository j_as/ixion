package com.ixion.client.renderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.*;
import java.util.*;

import com.ixion.client.GLObject;

import vecmath.*;

public class Shader implements GLObject {
	private static final List<Shader> shaders = new ArrayList<Shader>();

	public final static void create() {
		for (Shader shader : shaders) {
			shader.compile();
		}
	}

	public final static void destroy() {
		for (Shader shader : shaders) {
			shader.dispose();
		}
	}

	private String path;
	private int id;

	public static final Shader basicShader = new Shader("basic");
	public static final Shader lightShader = new Shader("light");
	public static final Shader hoverShader = new Shader("hover");
	public static final Shader textureShader = new Shader("texture");
	public static final Shader displayShader = new Shader("display");

	public static Shader currentShader;

	private Shader(String filename) {
		path = "/shaders/" + filename;
		shaders.add(this);
	}

	public void compile() {
		id = glCreateProgram();

		int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, load(".vs"));
		glCompileShader(vertexShader);

		if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) == GL_FALSE) {
			System.err.println("Unable to create vertex shader:");
			System.err.println(glGetShaderInfoLog(vertexShader, glGetShaderi(vertexShader, GL_INFO_LOG_LENGTH)));
			return;
		}

		int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, load(".fs"));
		glCompileShader(fragmentShader);

		if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == GL_FALSE) {
			System.err.println("Unable to create fragment shader:");
			System.err.println(glGetShaderInfoLog(fragmentShader, glGetShaderi(fragmentShader, GL_INFO_LOG_LENGTH)));
			return;
		}

		glAttachShader(id, vertexShader);
		glAttachShader(id, fragmentShader);
		glLinkProgram(id);

		if (glGetProgrami(id, GL_LINK_STATUS) == GL_FALSE) {
			System.err.println("Unable to link shader program! " + this);
			System.exit(0);
		}
	}

	private String load(String extension) {
		String code = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path + extension)));
			String line;
			while ((line = reader.readLine()) != null) {
				code = code + line + "\n";
			}
		} catch (Exception e) {
			System.err.println("Fail reading vertex shading code");
			e.printStackTrace();
		}

		return code;
	}

	public void enable() {
		glUseProgram(id);
		currentShader = this;
	}

	public void disable() {
		glUseProgram(0);
		currentShader = null;
	}

	public void setUniform(String name, float... values) {
		int location = glGetUniformLocation(id, name);

		if (values.length == 1) {
			glUniform1f(location, values[0]);
		} else if (values.length == 2) {
			glUniform2f(location, values[0], values[1]);
		} else if (values.length == 3) {
			glUniform3f(location, values[0], values[1], values[2]);
		} else if (values.length == 4) {
			glUniform4f(location, values[0], values[1], values[2], values[3]);
		} else {
			System.err.println("Enter between 1 and 4 values!");
		}
	}

	public void setUniform(String name, int... values) {
		int location = glGetUniformLocation(id, name);

		if (values.length == 1) {
			glUniform1i(location, values[0]);
		} else if (values.length == 2) {
			glUniform2i(location, values[0], values[1]);
		} else if (values.length == 3) {
			glUniform3i(location, values[0], values[1], values[2]);
		} else if (values.length == 4) {
			glUniform4i(location, values[0], values[1], values[2], values[3]);
		} else {
			System.err.println("Enter between 1 and 4 values!");
		}
	}

	public void setUniform(String name, Vec3 value) {
		int location = glGetUniformLocation(id, name);
		glUniform3f(location, value.x, value.y, value.z);
	}

	public void setUniform(String name, Mat4 value) {
		int location = glGetUniformLocation(id, name);
		glUniformMatrix4(location, false, value.export());
	}

	public void dispose() {
		glDeleteShader(id);
	}
}