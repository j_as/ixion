package com.ixion.client.renderer;

import java.util.*;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.*;

import static org.lwjgl.opengl.GL11.*;
import de.longor1996.util.objloader.IOBJOutput;
import de.longor1996.util.objloader.OBJLoader;
import de.longor1996.util.objloader.OBJLoader.OBJLoaderException;

public class Model implements IOBJOutput {
	private List<int[]> faces = new ArrayList<int[]>();
	private List<Vec3> normals = new ArrayList<Vec3>();
	private List<Vec3> vertices = new ArrayList<Vec3>();
	private List<Vec3> texCoords = new ArrayList<Vec3>();

	private Vec3 pos;
	private List<Collidable> collidables = new ArrayList<Collidable>();

	public Model(String filename, Vec3 pos) {
		this.pos = pos;

		try {
			OBJLoader ol = new OBJLoader();
			ol.setInput(new Scanner(getClass().getResourceAsStream("/models/" + filename + ".obj")));
			ol.setOutput(this);
			ol.setTessellate(true);
			ol.setNormalizeNormals(true);
			ol.setSubtractOneFromIndices(true);
			ol.setClampTextureCoordinates(false);
			ol.process();
		} catch (OBJLoaderException e) {
			e.printStackTrace();
		}
	}

	public void onProcessingDone(OBJLoader loader) {
		collidables = getColliders(new Vec3());
	}

	public List<Collidable> getColliders(Vec3 pos) {
		List<Collidable> collidables = new ArrayList<Collidable>();

		for (int i = 0; i < faces.size() - 2; i += 3) {
			int p0 = faces.get(i + 0)[0];
			int p1 = faces.get(i + 1)[0];
			int p2 = faces.get(i + 2)[0];

			Vec3 n0 = vertices.get(p0).clone().add(pos);
			Vec3 n1 = vertices.get(p1).clone().add(pos);
			Vec3 n2 = vertices.get(p2).clone().add(pos);

			collidables.add(new CollidablePolygon(n0, n1, n2));

			collidables.add(new CollidableLine(n0, n1));
			collidables.add(new CollidableLine(n1, n2));
			collidables.add(new CollidableLine(n2, n0));
		}
		return collidables;
	}

	public void onProcessingStart(OBJLoader loader) {
	}

	public void outputFace(int size, ArrayList<int[]> face) {
		/*
			face[0] = VERTEX INDEX
			face[1] = TEXCOORD INDEX
			face[2] = NORMAL INDEX
		 */

		faces.addAll(face);
	}

	public void outputMTLLibDefinition(String def) {
	}

	public void outputMaterialBind(String material) {
	}

	public void outputNormal(float x, float y, float z) {
		normals.add(new Vec3(x, y, z));
	}

	public void outputObjectGroup(String object) {
	}

	public void outputPolygonGroup(String poly) {
	}

	public void outputSmoothingGroup(int smoothing) {
	}

	public void outputTextureCoodinate(float x, float y, float z) {
		texCoords.add(new Vec3(x, y, z));
	}

	public void outputVertex(float x, float y, float z) {
		vertices.add(new Vec3(x, y, z).add(pos));
	}

	public boolean processLine(String line) {
		return false;
	}

	public void render(FrustumCuller culler, int color) {
		Tessellator t = Tessellator.instance;
		t.begin(GL_TRIANGLES);

		boolean[] noRender = new boolean[faces.size()];
		for (int i = 0; i < noRender.length - 2; i += 3) {
			if (culler == null) continue;
			int p0 = faces.get(i + 0)[0];
			int p1 = faces.get(i + 1)[0];
			int p2 = faces.get(i + 2)[0];

			Vec3 n0 = vertices.get(p0);
			Vec3 n1 = vertices.get(p1);
			Vec3 n2 = vertices.get(p2);

			boolean b0 = !culler.pointInFrustum(n0);
			boolean b1 = !culler.pointInFrustum(n1);
			boolean b2 = !culler.pointInFrustum(n2);

			noRender[i + 0] = b0 && b1 && b2;
			noRender[i + 1] = b0 && b1 && b2;
			noRender[i + 2] = b0 && b1 && b2;
		}

		for (int i = 0; i < faces.size(); i++) {
			if (noRender[i]) continue;

			int pp = 0;
			int vp = faces.get(i)[pp++];
			int tp = faces.get(i)[pp++];
			int np = faces.get(i)[pp++];

			Vec3 vertex = vertices.get(vp);
			Vec3 texCoord = texCoords.get(tp);
			Vec3 normal = normals.get(np);

			if (texCoord != null) texCoord = null;

			t.color(color);
			t.normal(normal);
			// t.tex(texCoord.x, texCoord.y);
			t.vertex(vertex);
		}
		t.end();
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.addAll(this.collidables);
	}
}