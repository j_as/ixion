package com.ixion.client.renderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.nio.*;

import vecmath.*;

import com.ixion.client.MemoryHandler;
import com.ixion.client.phys.AABB;

public class Tessellator {
	public static Tessellator instance = new Tessellator(2097152);

	private boolean tessellating = false;

	private FloatBuffer vertexBuffer;
	private FloatBuffer normalBuffer;
	private FloatBuffer colorBuffer;
	private FloatBuffer textureBuffer;

	private int pp = 0;

	private int drawMode = 0;
	private int texUnit;
	private float u, v;

	private float r, g, b, a;
	private float nx, ny, nz;
	private float xo, yo, zo;

	private boolean hasColor = false;
	private boolean hasNormal = false;
	private boolean hasTexture = false;

	private Tessellator(int size) {
		vertexBuffer = MemoryHandler.createFloatBuffer(size);
		normalBuffer = MemoryHandler.createFloatBuffer(size);
		colorBuffer = MemoryHandler.createFloatBuffer(size);
		textureBuffer = MemoryHandler.createFloatBuffer(size);

		reset();
	}

	public void offset(float xo, float yo, float zo) {
		this.xo = xo;
		this.yo = yo;
		this.zo = zo;
	}

	public void offset(Vec3 p) {
		offset(p.x, p.y, p.z);
	}

	public void color(float r, float g, float b, float a) {
		hasColor = true;
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public void color(float r, float g, float b) {
		color(r, g, b, 255);
	}

	public void color(int r, int g, int b, int a) {
		color(r / 255f, g / 255f, b / 255f, a / 255f);
	}

	public void color(int r, int g, int b) {
		color(r / 255f, g / 255f, b / 255f);
	}

	public void color(int color) {
		int a = color >> 24 & 0xff;
		int r = color >> 16 & 0xff;
		int g = color >> 8 & 0xff;
		int b = color & 0xff;
		color(r, g, b, a);
	}

	public void color(int color, int a) {
		int r = color >> 16 & 0xff;
		int g = color >> 8 & 0xff;
		int b = color & 0xff;
		color(r, g, b, a);
	}

	public void normal(float nx, float ny, float nz) {
		hasNormal = true;
		this.nx = nx;
		this.ny = ny;
		this.nz = nz;
	}

	public void normal(Vec3 n) {
		normal(n.x, n.y, n.z);
	}

	public void tex(int texUnit, float u, float v) {
		hasTexture = true;
		this.texUnit = texUnit;
		this.u = u;
		this.v = v;
	}

	public void tex(float u, float v) {
		tex(0, u, v);
	}

	public void tex(Vec2 uv) {
		tex(uv.x, uv.y);
	}

	private void reset() {
		tessellating = false;
		nx = ny = nz = xo = yo = zo = r = g = b = a = 0;
		u = v = 0;
		texUnit = 0;
		drawMode = 0;
	}

	public void begin() {
		begin(GL_LINE_BIT);
	}

	public void begin(int mode) {
		if (tessellating) {
			throw new RuntimeException("Tessellator is already Tessellating!");
		}

		reset();
		pp = 0;
		drawMode = mode;
		tessellating = true;
	}

	public void end() {
		if (pp == 0) {
			vertexBuffer.rewind();
			colorBuffer.rewind();
			normalBuffer.rewind();
			textureBuffer.rewind();

			reset();
			return;
		}

		vertexBuffer.rewind();
		colorBuffer.rewind();
		normalBuffer.rewind();
		textureBuffer.rewind();

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, 3 << 2, vertexBuffer);

		if (hasColor) {
			glEnableClientState(GL_COLOR_ARRAY);
			glColorPointer(4, 4 << 2, colorBuffer);
		}

		if (hasNormal) {
			glEnableClientState(GL_NORMAL_ARRAY);
			glNormalPointer(3 << 2, normalBuffer);
		}

		if (hasTexture) {
			glActiveTexture(GL_TEXTURE0 + texUnit);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, 2 << 2, textureBuffer);
		}

		glDrawArrays(drawMode, 0, pp);

		glDisableClientState(GL_VERTEX_ARRAY);
		if (hasColor) glDisableClientState(GL_COLOR_ARRAY);
		if (hasNormal) glDisableClientState(GL_NORMAL_ARRAY);
		if (hasTexture) glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		reset();
	}

	public void vertex(float x, float y, float z) {
		float xx = (int) ((x + xo) * 128f) / 128f;
		float yy = (int) ((y + yo) * 128f) / 128f;
		float zz = (int) ((z + zo) * 128f) / 128f;

		vertexBuffer.put(xx).put(yy).put(zz);
		pp++;

		if (hasNormal) {
			normalBuffer.put(nx).put(ny).put(nz);
		}
		if (hasColor) {
			colorBuffer.put(r).put(g).put(b).put(a);
		}
		if (hasTexture) {
			textureBuffer.put(u).put(v);
		}
	}

	public void vertex(Vec3 r) {
		vertex(r.x, r.y, r.z);
	}

	public void vertexUV(float x, float y, float z, float u, float v) {
		tex(0, u, v);
		vertex(x, y, z);
	}

	public void vertex(float x, float y) {
		vertex(x, y, 0);
	}

	public void vertex(Vec2 r) {
		vertex(r.x, r.y);
	}

	public void vertexUV(float x, float y, float u, float v) {
		tex(0, u, v);
		vertex(x, y, 0);
	}

	public void line(float x0, float y0, float z0, float x1, float y1, float z1) {
		float Nx = y1 * z0 - z1 * y0;
		float Ny = z1 * x0 - x1 * z0;
		float Nz = x1 * y0 - y1 * x0;

		float dd = (float) Math.sqrt(Nx * Nx + Ny * Ny + Nz * Nz);

		Nx /= dd;
		Ny /= dd;
		Nz /= dd;

		normal(Nx, Ny, Nz);
		vertex(x0, y0, z0);
		vertex(x1, y1, z1);
	}

	public void line(Vec3 v0, Vec3 v1) {
		line(v0.x, v0.y, v0.z, v1.x, v1.y, v1.z);
	}

	public void triangle(float x0, float y0, float z0, float x1, float y1, float z1, float x2, float y2, float z2) {
		float Ux = x1 - x0;
		float Uy = y1 - y0;
		float Uz = z1 - z0;

		float Vx = x2 - x0;
		float Vy = y2 - y0;
		float Vz = z2 - z0;

		float Nx = Uy * Vz - Uz * Vy;
		float Ny = Uz * Vx - Ux * Vz;
		float Nz = Ux * Vy - Uy * Vx;

		float dd = (float) Math.sqrt(Nx * Nx + Ny * Ny + Nz * Nz);

		Nx /= dd;
		Ny /= dd;
		Nz /= dd;

		normal(Nx, Ny, Nz);
		vertex(x0, y0, z0);
		vertex(x1, y1, z1);
		vertex(x2, y2, z2);
	}

	public void triangle(Vec3 a, Vec3 b, Vec3 c) {
		triangle(a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z);
	}

	public void quad(float x0, float y0, float z0, float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
		float Ux = x1 - x0;
		float Uy = y1 - y0;
		float Uz = z1 - z0;

		float Vx = x3 - x0;
		float Vy = y3 - y0;
		float Vz = z3 - z0;

		float Nx = Uy * Vz - Uz * Vy;
		float Ny = Uz * Vx - Ux * Vz;
		float Nz = Ux * Vy - Uy * Vx;

		float dd = (float) Math.sqrt(Nx * Nx + Ny * Ny + Nz * Nz);

		Nx /= dd;
		Ny /= dd;
		Nz /= dd;

		normal(Nx, Ny, Nz);
		vertex(x0, y0, z0);
		vertex(x1, y1, z1);
		vertex(x2, y2, z2);
		vertex(x3, y3, z3);
	}

	public void quad(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3) {
		quad(v0.x, v0.y, v0.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z);
	}

	public void cube(float x0, float y0, float z0, float x1, float y1, float z1) {
		quad(x0, y0, z0, x1, y0, z0, x1, y0, z1, x0, y0, z1);
		quad(x0, y1, z1, x1, y1, z1, x1, y1, z0, x0, y1, z0);
		quad(x1, y1, z0, x1, y1, z1, x1, y0, z1, x1, y0, z0);
		quad(x0, y1, z1, x0, y1, z0, x0, y0, z0, x0, y0, z1);
		quad(x0, y0, z1, x1, y0, z1, x1, y1, z1, x0, y1, z1);
		quad(x0, y1, z0, x1, y1, z0, x1, y0, z0, x0, y0, z0);
	}

	public void cube(Vec3 v0, Vec3 v1) {
		cube(v0.x, v0.y, v0.z, v1.x, v1.y, v1.z);
	}

	public void cube(AABB bb) {
		float x0 = bb.x;
		float y0 = bb.y;
		float z0 = bb.z;
		float x1 = x0 + bb.xs;
		float y1 = y0 + bb.ys;
		float z1 = z0 + bb.zs;
		cube(x0, y0, z0, x1, y1, z1);
	}

	public void sphere(float xo, float yo, float zo, float lats, float longs, float radius) {
		for (int i = 0; i <= lats; i++) {
			float lat0 = (float) (Math.PI * (-0.5 + (i - 1.0) / lats));
			float z0 = (float) Math.sin(lat0);
			float zr0 = (float) Math.cos(lat0);

			float lat1 = (float) (Math.PI * (-0.5 + (float) i / lats));
			float z1 = (float) Math.sin(lat1);
			float zr1 = (float) Math.cos(lat1);

			for (int j = 0; j < longs; j++) {
				float lng = (float) (2 * Math.PI * (j - 1.0) / longs);
				float x = (float) Math.cos(lng);
				float y = (float) Math.sin(lng);

				offset(xo, yo, zo);
				normal(x * zr0, y * zr0, z0);
				vertex(x * zr0 * radius, y * zr0 * radius, z0 * radius);
				normal(x * zr1, y * zr1, z1);
				vertex(x * zr1 * radius, y * zr1 * radius, z1 * radius);
				offset(0, 0, 0);
			}
		}
	}

	public void sphere(Vec3 pos, float lats, float longs, float radius) {
		sphere(pos.x, pos.y, pos.z, lats, longs, radius);
	}
}