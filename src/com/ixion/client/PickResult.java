package com.ixion.client;

import vecmath.Vec3;

import com.ixion.client.phys.Collidable;

public class PickResult {
	public Vec3 pos;
	public Collidable collidable;

	public PickResult(Vec3 pos, Collidable collidable) {
		this.pos = pos;
		this.collidable = collidable;
	}
}