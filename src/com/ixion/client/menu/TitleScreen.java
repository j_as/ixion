package com.ixion.client.menu;

import java.io.*;
import java.util.*;

import org.lwjgl.opengl.Display;

import static org.lwjgl.opengl.GL11.*;
import vecmath.Vec3;

import com.ixion.client.*;
import com.ixion.client.game.GameScreen;
import com.ixion.client.planet.*;
import com.ixion.client.menu.gui.Button;
import com.ixion.client.renderer.*;

public class TitleScreen extends Screen {
	private List<String> splashes = new ArrayList<String>();
	private List<Button> buttons = new ArrayList<Button>();
	private Random random = new Random();
	private String splashMessage = "";
	private Button[] lastButtons = new Button[5];

	private int displayList, spaceList;
	
	private float time;
	private Vec3 viewPos = new Vec3(0, 0, 30f).mul(-1);
	private int translationType;

	private float splashFadeTime = 0;
	private float fade = 255f;
	private float logoTime = 400f;
	private float dropLogoTime = 0;
	private int logoColor;

	public TitleScreen() {
		logoColor = random.nextInt() == 0 ? hex(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)) : 0xffffff;
		translationType = random.nextInt(3);

		try {
			BufferedReader br = new BufferedReader(new FileReader("doc/splashes.txt"));
			StringBuilder paragraph = null;

			int i = -1;
			while ((i = br.read()) != -1) {
				char c = (char) i;
				if (c == '*') {
					if (paragraph != null) splashes.add(paragraph.toString());
					paragraph = new StringBuilder();
					continue;
				}

				paragraph.append(c);
			}

			if (paragraph != null) splashes.add(paragraph.toString());
			br.close();
		} catch (IOException e) {
			splashes.add("Error loading splashes.");
			e.printStackTrace();
		}

		generateNewSplashMessage();
	}

	public void init(Ixion ixion) {
		super.init(ixion);

		farClip = 100f;

		int pp = 0;
		float s = 30f;
		float tt = 250f;
		lastButtons[pp++] = new Button("Play Singleplayer", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Play Multiplayer", 100, tt - pp * s).isActive(false);
		lastButtons[pp++] = new Button("Credits", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Options", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Quit", 100, tt - pp * s);

		for (int i = 0; i < lastButtons.length; i++) {
			buttons.add(lastButtons[i]);
		}

		displayList = glGenLists(1);
		spaceList = glGenLists(1);

		boolean containsStar = random.nextBoolean();

		glNewList(displayList, GL_COMPILE);
		float xx = 10f + (random.nextFloat() * 4f);
		float yy = random.nextFloat() - random.nextFloat();
		float zz = 0;
		float rr = 1f + random.nextFloat() * 5f;
		if (containsStar) {
			new Star(new Vec3(xx, yy, zz), rr).render(null);
		} else {
			new Planet(new Vec3(xx, yy, zz), rr).render(null);
		}

		float xxm = xx + (1 + rr * random.nextFloat() * 2f);
		float yym = yy + (1 + rr * random.nextFloat() * 2f);
		float zzm = zz + (1 + rr * random.nextFloat() * 2f);
		float rrm = rr / (4 - random.nextFloat() * 2f);
		if (containsStar) {
			if (random.nextInt(3) == 0) new Planet(new Vec3(xxm, yym, zzm), rrm).render(null);
		} else {
			if (random.nextInt(3) == 0) new Moon(new Vec3(xxm, yym, zzm), rrm).render(null);
		}
		glEndList();

		generateStarList();
	}

	private void generateStarList() {
		Tessellator t = Tessellator.instance;

		int screenWidth = Display.getWidth();
		int screenHeight = Display.getHeight();

		int stars = random.nextInt(400) + 200;

		glNewList(spaceList, GL_COMPILE);
		for (int i = 0; i < stars; i++) {
			float x = random.nextFloat() * screenWidth;
			float y = random.nextFloat() * screenHeight;
			float z = 0;
			float r = random.nextFloat() * 3f;

			float c = random.nextFloat();
			if (c > 0.85f) c /= 2f;

			t.begin(GL_QUADS);
			t.color(c, c, c);
			t.quad(x, y, z, x + r, y, z, x + r, y + r, z, x, y + r, z);
			t.end();
		}
		glEndList();
	}

	public void onClose() {
		glDeleteLists(displayList, 1);
		glDeleteLists(spaceList, 1);
	}

	private void generateNewSplashMessage() {
		String splashMessage = splashes.get(random.nextInt(splashes.size()));
		this.splashMessage = splashMessage;
	}

	public void tick(double delta) {
		time += 0.2f;
		splashFadeTime += (60f / splashMessage.length());
		if (dropLogoTime < logoTime) dropLogoTime += 2.5f;

		float regen = splashFadeTime % fade;
		if (regen > 0 && regen <= 1f) {
			generateNewSplashMessage();
		}
	}

	public void renderScene() {
		modelMatrix.translate(viewPos);

		float sp = translationType * 150f;
		float rot = 1 + (time % 10000f / sp);
		if (rot > 39) time = 0;

		if (translationType == 0) modelMatrix.rotX(rot);
		if (translationType == 1) modelMatrix.rotY(rot);
		if (translationType == 2) modelMatrix.rotZ(rot);

		Shader.basicShader.enable();
		updateUniforms();
		glCallList(displayList);
		Shader.basicShader.disable();
	}

	public void renderHud() {
		Shader.basicShader.enable();
		updateUniforms();
		glDisable(GL_CULL_FACE);
		glCallList(spaceList);
		glEnable(GL_CULL_FACE);
		Shader.basicShader.disable();

		Shader.textureShader.enable();
		updateUniforms();
		super.renderHud();

		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).render(renderer);
		}
		int color = hex((int) (splashFadeTime % fade), 255, 255, 255);

		renderer.renderString(splashMessage, color, screenWidth * 0.5f, 220, 0.4f, 0.4f);
		renderLogo();
		Shader.textureShader.disable();
	}

	private void renderLogo() {
		float x = (screenWidth / 2f) - (Texture.ixionLogoTexture.w / 2f);
		float y = screenHeight - dropLogoTime;
		int a = (int) (dropLogoTime / 1.6) + 5;
		renderer.texturedTransparentQuad(Texture.ixionLogoTexture, x, y, logoColor, a);
	}

	public void onClick(int mb) {
		for (int i = 0; i < buttons.size(); i++) {
			Button button = buttons.get(i);
			Screen screen = checkSelected(button);

			if (screen != null) {
				setScreen(screen);
				return;
			}
		}
	}

	private Screen checkSelected(Button button) {
		boolean clicked = button.hasClicked(0);

		int pp = 0;
		if (button == lastButtons[pp++] && clicked) return new GameScreen();
		if (button == lastButtons[pp++] && clicked) return new ServerScreen();
		if (button == lastButtons[pp++] && clicked) return new CreditsScreen();
		if (button == lastButtons[pp++] && clicked) return new OptionsScreen(this);
		if (button == lastButtons[pp++] && clicked) exitGame();
		return null;
	}
}