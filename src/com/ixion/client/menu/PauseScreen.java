package com.ixion.client.menu;

import static org.lwjgl.opengl.GL11.*;

import java.util.*;

import org.lwjgl.input.Keyboard;

import vecmath.*;

import com.ixion.client.*;
import com.ixion.client.game.*;
import com.ixion.client.menu.gui.Button;
import com.ixion.client.renderer.*;
import com.ixion.client.ship.Ship;

public class PauseScreen extends Screen {
	private GameScreen gameScreen;
	private Button[] lastButtons = new Button[3];
	private List<Button> buttons = new ArrayList<Button>();

	private Ship ship;

	public PauseScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
		ship = gameScreen.ship;

		gameScreen.ship.setDirty();
	}

	public void init(Ixion ixion) {
		super.init(ixion);

		int pp = 0;
		float s = 30f;
		float tt = 250f;
		lastButtons[pp++] = new Button("Return to game", 100, tt - s * pp);
		lastButtons[pp++] = new Button("Options", 100, tt - s * pp);
		lastButtons[pp++] = new Button("Save and quit", 100, tt - s * pp);

		for (int i = 0; i < lastButtons.length; i++) {
			buttons.add(lastButtons[i]);
		}
	}

	public void renderScene() {
		viewMatrix.rotX(gameScreen.xRot);
		viewMatrix.rotY(gameScreen.yRot);
		viewMatrix.translate(gameScreen.player.getEyePos().mul(-1f));

		Mat4 normalMatrix = createNormalMatrix();

		Shader.lightShader.enable();
		updateUniforms();
		glEnable(GL_BLEND);
		for (int i = 0; i < gameScreen.lights.size(); i++) {
			gameScreen.lights.get(i).updateUniforms(normalMatrix);

			if (i > 0) glBlendFunc(GL_ONE, GL_ONE);
			glCallList(gameScreen.sceneList);
		}
		glDisable(GL_BLEND);
		Shader.lightShader.disable();

		Shader.displayShader.enable();
		updateUniforms();
		gameScreen.cpu.getAvailableMonitor().render();
		Shader.displayShader.disable();
	}

	public void renderHud() {
		Shader.basicShader.enable();
		updateUniforms();
		renderer.transparentQuad(60, 0, 300, screenHeight, 0, 100);
		Shader.basicShader.disable();

		Shader.textureShader.enable();
		updateUniforms();
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).render(renderer);
		}
		super.renderHud();
		renderer.texturedTransparentQuad(Texture.ixionLogoTextureSmall, 10, screenHeight - 400, 0xffffff, 255);
		Shader.textureShader.disable();
	}

	public void onClick(int mb) {
		for (int i = 0; i < buttons.size(); i++) {
			Button button = buttons.get(i);
			Screen screen = checkSelected(button);
			if (screen != null) {
				setScreen(screen);
				return;
			}
		}
	}

	private Screen checkSelected(Button button) {
		boolean clicked = button.hasClicked(0);

		if (button == lastButtons[0] && clicked) {
			return gameScreen;
		} else if (button == lastButtons[1] && clicked) {
			ship.save(Ship.mainSave);
			return new OptionsScreen(this);
		} else if (button == lastButtons[2] && clicked) {
			ship.save(Ship.mainSave);
			exitGame();
		}

		return null;
	}

	public void keyPressed(int key) {
		if (key == Keyboard.KEY_ESCAPE) setScreen(gameScreen);
	}
}