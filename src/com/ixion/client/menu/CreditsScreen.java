package com.ixion.client.menu;

import static org.lwjgl.opengl.GL11.*;

import java.io.*;
import java.util.*;

import org.lwjgl.opengl.*;

import com.ixion.client.*;
import com.ixion.client.renderer.*;

public class CreditsScreen extends Screen {
	private int displayList;

	private List<String> credits = new ArrayList<String>();
	private float time = 0;
	private float fade = 255f;
	private int namePointer = -1;
	private Random random = new Random();
	private boolean skipToTileScreen = false;

	public CreditsScreen() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("doc/credits.txt"));
			StringBuilder paragraph = null;

			int i = -1;
			while ((i = br.read()) != -1) {
				char c = (char) i;
				if (c == '*') {
					if (paragraph != null) credits.add(paragraph.toString());
					paragraph = new StringBuilder();
					continue;
				}

				paragraph.append(c);
			}

			if (paragraph != null) credits.add(paragraph.toString());
			br.close();
		} catch (IOException e) {
			credits.add("Error loading splashes.");
			e.printStackTrace();
		}
	}

	public void init(Ixion ixion) {
		super.init(ixion);
		displayList = glGenLists(1);
		generateStarList();
	}
	
	public void onClose() {
		glDeleteLists(displayList, 1);
	}

	private void generateStarList() {
		Tessellator t = Tessellator.instance;
		int w = Display.getWidth();
		int h = Display.getHeight();

		glNewList(displayList, GL_COMPILE);
		{
			for (int i = 0; i < 500; i++) {
				float x = random.nextFloat() * w;
				float y = random.nextFloat() * h;
				float z = 0;
				float r = random.nextFloat() * 3f;

				float c = random.nextFloat();
				if (c > 0.9f) c /= 2f;

				t.begin(GL_QUADS);
				t.color(c, c, c);
				t.quad(x, y, z, x + r, y, z, x + r, y + r, z, x, y + r, z);
				t.end();
			}
		}
		glEndList();
	}

	public void tick(double delta) {
		time++;

		float regen = time % fade;
		if (regen > 0 && regen <= 1.0) {
			namePointer++;
		}

		if (namePointer == credits.size()) skipToTileScreen = true;
		if (skipToTileScreen) {
			setScreen(new TitleScreen());
		}
	}

	public void renderHud() {
		if (Display.wasResized()) {
			generateStarList();
		}

		Shader.basicShader.enable();
		updateUniforms();
		glCallList(displayList);
		Shader.basicShader.disable();

		String line = credits.get(namePointer);
		int alpha = (int) (time % fade);
		int color = hex(alpha, 255, 255, 255);
		float s = alpha / 200f;
		float sw = renderer.textRenderer.getWidth(line) / 3f * s;
		float xx = screenWidth / 2f - sw;
		float yy = screenHeight / 2f;

		Shader.textureShader.enable();
		updateUniforms();
		renderer.renderString(line, color, xx, yy, s, s);
		super.renderHud();
		Shader.textureShader.disable();
	}

	public void keyPressed(int eventKey) {
		skipToTileScreen = true;
	}
}