package com.ixion.client.menu;

import static org.lwjgl.opengl.GL11.*;
import java.util.*;

import com.ixion.client.*;
import com.ixion.client.menu.gui.Button;
import com.ixion.client.renderer.Shader;

public class OptionsScreen extends Screen {
	private int hudList;

	private Screen lastScreen;
	private Button[] lastButtons = new Button[5];
	private List<Button> buttons = new ArrayList<Button>();

	public OptionsScreen(Screen lastScreen) {
		this.lastScreen = lastScreen;
	}

	public void init(Ixion ixion) {
		super.init(ixion);

		int pp = 0;
		float s = 30f;
		float tt = 400f;

		lastButtons[pp++] = new Button("Render quality: normal", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Fullscreen on launch: no", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Render distance: normal", 100, tt - pp * s);

		tt = 250f;
		lastButtons[pp++] = new Button("Save", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Cancel", 100, tt - pp * s);

		for (int i = 0; i < lastButtons.length; i++) {
			buttons.add(lastButtons[i]);
		}

		hudList = glGenLists(1);
	}

	public void onClose() {
		glDeleteLists(hudList, 1);
		lastScreen = null;
	}

	public void renderHud() {
		glNewList(hudList, GL_COMPILE);
		Shader.textureShader.enable();
		updateUniforms();
		renderer.renderString("Options", 100, screenHeight - 200);
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).render(renderer);
		}

		Shader.textureShader.disable();
		glEndList();

		glCallList(hudList);
	}

	public void onClick(int mb) {
		for (int i = 0; i < buttons.size(); i++) {
			Button button = buttons.get(i);

			Screen screen = checkSelected(button);
			if (screen != null) {
				setScreen(screen);
				return;
			} else {
				checkOptions(button);
			}
		}
	}

	private void checkOptions(Button button) {
	}

	private Screen checkSelected(Button button) {
		boolean clicked = button.hasClicked(0);
		if (button == lastButtons[3] && clicked) return lastScreen;
		if (button == lastButtons[4] && clicked) return lastScreen;
		return null;
	}
}