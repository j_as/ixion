package com.ixion.client.menu;

import java.util.*;

import org.lwjgl.input.Keyboard;

import com.ixion.client.*;
import com.ixion.client.menu.gui.Button;
import com.ixion.client.menu.gui.TextEntry;
import com.ixion.client.renderer.Shader;

public class ServerScreen extends Screen {
	private Button[] lastButtons = new Button[2];
	private TextEntry portEntry;
	private List<Button> buttons = new ArrayList<Button>();

	public void init(Ixion ixion) {
		super.init(ixion);

		portEntry = new TextEntry("Enter port: ", 100, 350);
		portEntry.ignoreLetters = true;

		int pp = 0;
		float s = 30f;
		float tt = 250f;
		lastButtons[pp++] = new Button("Join Server", 100, tt - pp * s);
		lastButtons[pp++] = new Button("Go back", 100, tt - pp * s);

		for (int i = 0; i < lastButtons.length; i++) {
			buttons.add(lastButtons[i]);
		}
	}

	public void tick(double delta) {
	}

	public void renderHud() {
		Shader.textureShader.enable();
		updateUniforms();
		renderer.renderString("Join a local game", 100, screenHeight - 200);
		portEntry.render(renderer);

		for (int i = 0; i < buttons.size(); i++) {
			Button button = buttons.get(i);
			button.render(renderer);
		}

		super.renderHud();
		Shader.textureShader.disable();
	}

	public void keyPressed(int eventKey) {
		portEntry.keyPressed(eventKey, Keyboard.getEventCharacter());
	}

	public void keyReleased(int eventKey) {
		portEntry.keyReleased(eventKey, Keyboard.getEventCharacter());
	}

	public void onClick(int mb) {
		for (int i = 0; i < buttons.size(); i++) {
			Button button = buttons.get(i);
			Screen screen = checkSelected(button);
			if (screen != null) {
				setScreen(screen);
				return;
			}
		}
	}

	private Screen checkSelected(Button button) {
		boolean clicked = button.hasClicked(0);
		if (button == lastButtons[0] && clicked) return null;
		if (button == lastButtons[1] && clicked) return new TitleScreen();
		return null;
	}
}