package com.ixion.client.menu;

import com.ixion.client.*;
import com.ixion.client.renderer.Shader;

public class LoadingScreen extends Screen {
	private int time;

	public void init(Ixion ixion) {
		super.init(ixion);
	}

	public void tick(double delta) {
		time++;
	}

	public void renderHud() {
		Shader.textureShader.enable();
		updateUniforms();
		super.renderHud();

		int tt = time / 25 % 4;
		String msg = "";

		if (tt == 0) msg = "Loading";
		if (tt == 1) msg = "Loading.";
		if (tt == 2) msg = "Loading..";
		if (tt == 3) msg = "Loading...";
		float ww = screenWidth / 2f;
		float hh = screenHeight / 2f;
		ww -= renderer.textRenderer.getWidth(msg) / 2f;

		renderer.renderString(msg, ww, hh);
		Shader.textureShader.disable();
	}
}