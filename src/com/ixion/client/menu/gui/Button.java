package com.ixion.client.menu.gui;

import org.lwjgl.input.Mouse;

import com.ixion.client.renderer.Renderer;

public class Button {
	public String title;
	public float x, y;
	public float w, h;

	private boolean deactive = false;

	private final int color = 0xff7d7d7d;
	private final int hoverColor = 0xffffffff;
	private int color1 = color;

	public Button(String title, float x, float y) {
		this.title = title;
		this.x = x;
		this.y = y;
		w = title.length() * 12;
		h = 20;
	}

	public Button isActive(boolean b) {
		deactive = !b;
		return this;
	}

	public boolean checkHovered() {
		int mx = Mouse.getX();
		int my = Mouse.getY();
		color1 = color;

		if (mx >= x && my >= y && mx <= x + w && my <= y + h && !deactive) {
			onHover();
			return true;
		}
		return false;
	}

	public boolean hasClicked(int mb) {
		if (checkHovered() && Mouse.isButtonDown(mb)) {
			onClick();
			return true;
		}
		return false;
	}

	public void onHover() {
		color1 = hoverColor;
	}

	public void onClick() {
	}

	public void render(Renderer renderer) {
		checkHovered();
		renderer.renderString(title, color1, x, y, 0.6f, 0.6f);
	}
}