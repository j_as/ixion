package com.ixion.client.menu.gui;

import org.lwjgl.input.Keyboard;

import com.ixion.client.renderer.Renderer;

public class TextEntry {
	public String entry = "";
	public String label = "";
	public float x, y;

	public boolean ignoreLetters = false;
	public int color = 0xffffffff;

	public TextEntry(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public TextEntry(String label, float x, float y) {
		this.label = label;
		this.x = x;
		this.y = y;
	}

	public void render(Renderer renderer) {
		renderer.renderString(label + entry, color, x, y, 0.6f, 0.6f);
	}

	private void removeLast() {
		if (entry.length() > 0) {
			entry = entry.substring(0, entry.length() - 1);
		}
	}

	public int getNumber() {
		return Integer.parseInt(entry.trim());
	}

	public void keyPressed(int eventKey, char c) {
		if (c == Keyboard.KEY_NONE) return;

		if (eventKey == Keyboard.KEY_BACK) {
			removeLast();
			return;
		}

		entry += c;
		if (ignoreLetters) entry = entry.replaceAll("[^\\d.]", "");
	}

	public void keyReleased(int eventKey, char c) {
	}
}