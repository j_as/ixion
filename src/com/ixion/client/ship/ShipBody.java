package com.ixion.client.ship;

import java.util.*;
import static org.lwjgl.opengl.GL11.*;

import vecmath.Vec3;

import com.ixion.client.*;
import com.ixion.client.phys.*;
import com.ixion.client.renderer.*;

public class ShipBody {
	public Ship ship;

	private Model interior, exterior, engine;
	private Model windows;
	private Vec3 pos;

	public ShipBody(Ship ship) {
		this.ship = ship;

		pos = ship.bb.getMin().add(12.38f, 0, 7.5f);
		interior = new Model("ship/interior", pos);
		exterior = new Model("ship/exterior", pos);
		engine = new Model("ship/engine", pos);
		windows = new Model("ship/windows", pos);
	}

	public void render(FrustumCuller culler) {
		if (pos != ship.bb.getMin().add(12.38f, 0, 7.5f)) {
			pos = ship.bb.getMin().add(12.38f, 0, 7.5f);
		}

		interior.render(culler, 0xffffff);
		exterior.render(culler, 0xffffff);
		engine.render(culler, 0x8a0000);

		// renderWindows(null);
	}

	private void renderWindows(FrustumCuller culler) {
		glDisable(GL_CULL_FACE);
		windows.render(culler, 0xffffff);
		glEnable(GL_CULL_FACE);
	}

	public List<Collidable> getCollidables() {
		return interior.getColliders(pos);
	}
}