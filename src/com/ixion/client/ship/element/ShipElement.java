package com.ixion.client.ship.element;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.phys.AABB;
import com.ixion.client.phys.Collidable;
import com.ixion.client.ship.Ship;

public class ShipElement {
	public Ship ship;
	public AABB bb = new AABB();
	
	public ShipElement(Ship ship) {
		this.ship = ship;
	}

	public void tick(double delta) {
	}

	public void render() {
	}

	public void renderSelectedEffect() {
	}

	public void addCollidables(List<Collidable> collidables) {
	}

	public void addPickables(List<Collidable> pickables) {
	}

	public float getPowerDrainage() {
		return 0;
	}

	public void move(Vec3 motion) {
	}
}