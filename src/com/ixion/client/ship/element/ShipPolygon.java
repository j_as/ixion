package com.ixion.client.ship.element;

import static org.lwjgl.opengl.GL11.*;

import java.util.*;

import vecmath.Vec3;

import com.ixion.client.phys.*;
import com.ixion.client.renderer.Tessellator;
import com.ixion.client.ship.Ship;

public class ShipPolygon extends ShipElement {
	private class PickablePolygon extends CollidablePolygon {
		private int side;
		private Vec3 startDragPos;
		private int dragMode = -1;

		public PickablePolygon(int side, Vec3... pts) {
			super(pts);
			this.side = side;
		}

		public void onHovered(Vec3 pos) {
			setSelected(side);
		}

		public void onClick(int mb, Vec3 pos) {
			if (mb == 0) setCornerMask(pos);
		}

		private void setCornerMask(Vec3 pos) {
			for (int i = 0; i < 8; i++) {
				float dd = pos.distanceTo(corners[i]);

				if (dd <= NEAREST_INTERSECTION_DIST) {
					noCorner[i] ^= true;
					updateShape();
					continue;
				}
			}
		}

		public void startDrag(Vec3 startDragPos, boolean draging, boolean tabDown) {
			this.startDragPos = startDragPos.clone();

			if (draging && tabDown) dragMode = 0;
			else if (draging) dragMode = 1;
		}

		public void drag(Vec3 currentDragPos) {
			AABB newBB = new AABB();
			if (dragMode == 0) resizePolygon(newBB, currentDragPos);
			if (dragMode == 1) movePolygon(newBB, currentDragPos);

			trySetBB(newBB);
			startDragPos = currentDragPos.clone();
		}

		private void resizePolygon(AABB newBB, Vec3 currentDragPos) {
			float diff = 0;
			float dragConstant = 1.5f;

			if (side == SIDE_UP || side == SIDE_DOWN) {
				diff = (currentDragPos.y - startDragPos.y) * dragConstant;
			}

			if (side == SIDE_NORTH || side == SIDE_SOUTH) {
				diff = (currentDragPos.z - startDragPos.z) * dragConstant;
			}

			if (side == SIDE_EAST || side == SIDE_WEST) {
				diff = (currentDragPos.x - startDragPos.x) * dragConstant;
			}

			if (side == SIDE_UP) newBB.set(bb.x, bb.y, bb.z, bb.xs, bb.ys + diff, bb.zs);
			if (side == SIDE_DOWN) newBB.set(bb.x, bb.y + diff, bb.z, bb.xs, bb.ys - diff, bb.zs);
			if (side == SIDE_NORTH) newBB.set(bb.x, bb.y, bb.z + diff, bb.xs, bb.ys, bb.zs - diff);
			if (side == SIDE_EAST) newBB.set(bb.x, bb.y, bb.z, bb.xs + diff, bb.ys, bb.zs);
			if (side == SIDE_SOUTH) newBB.set(bb.x, bb.y, bb.z, bb.xs, bb.ys, bb.zs + diff);
			if (side == SIDE_WEST) newBB.set(bb.x + diff, bb.y, bb.z, bb.xs - diff, bb.ys, bb.zs);
		}

		private void movePolygon(AABB newBB, Vec3 currentDragPos) {
			float xDrag = (currentDragPos.x - startDragPos.x);
			float yDrag = (currentDragPos.y - startDragPos.y);
			float zDrag = (currentDragPos.z - startDragPos.z);

			newBB.set(bb.x + xDrag, bb.y + yDrag, bb.z + zDrag, bb.xs, bb.ys, bb.zs);
		}

		public void stopDrag() {
			dragMode = -1;
		}
	}

	private static final float NEAREST_INTERSECTION_DIST = 0.1f;

	private static final int SIDE_UP = 0;
	private static final int SIDE_DOWN = 1;
	private static final int SIDE_NORTH = 2;
	private static final int SIDE_EAST = 3;
	private static final int SIDE_SOUTH = 4;
	private static final int SIDE_WEST = 5;

	private PickablePolygon[] pickSides = new PickablePolygon[6];
	private List<CollidablePolygon> polygons = new ArrayList<CollidablePolygon>();

	private Vec3 uc0, uc1, uc2, uc3;
	private Vec3 vc0, vc1, vc2, vc3;

	private Vec3[] corners = new Vec3[8];
	private boolean[] noCorner = new boolean[8];
	private int selectedSide = -1;

	private int color = 0xffffffff;

	private List<Collidable> collidables = new ArrayList<Collidable>();
	private List<Collidable> pickables = new ArrayList<Collidable>();

	public ShipPolygon(Ship ship, float x, float y, float z, float xs, float ys, float zs, int color) {
		super(ship);
		bb.set(x, y, z, xs, ys, zs);
		this.color = color;
		updateShape();
	}

	public void move(Vec3 m) {
		AABB newBB = new AABB(bb);
		newBB.x += m.x;
		newBB.y += m.y;
		newBB.z += m.z;
		trySetBB(newBB);
	}

	private void setSelected(int selectedSide) {
		this.selectedSide = selectedSide;
		ship.currentSelected = this;
	}

	private void trySetBB(AABB newBB) {
		if (newBB.xs <= 0.1 || newBB.ys <= 0.1 || newBB.zs <= 0.1) return;
		if (!ship.isFree(newBB, this)) return;
		bb.set(newBB);
		updateShape();
	}

	public void render() {
		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);

		for (int i = 0; i < polygons.size(); i++) {
			Vec3[] vs = polygons.get(i).points;
			t.color(color);
			if (vs.length == 4) t.quad(vs[0], vs[1], vs[2], vs[3]);
			if (vs.length == 3) t.quad(vs[0], vs[1], vs[2], vs[2]);
		}

		t.end();
	}

	public void renderSelectedEffect() {
		Tessellator t = Tessellator.instance;
		t.begin(GL_LINES);
		t.color(0x9e9e9e);
		for (int i = 0; i < 6; i++) {
			Vec3[] edge = pickSides[i].points;

			int cc = 0;
			t.line(edge[cc++], edge[cc]);
			t.line(edge[cc++], edge[cc]);
			t.line(edge[cc++], edge[cc]);
			t.line(edge[cc = 0], edge[cc]);
		}
		t.end();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		int hoverColor = 0xff0000cc;
		int color = (int) (hoverColor * ((System.currentTimeMillis() / 16) / 20 % 2));
		for (int pass = 0; pass < 2; pass++) {
			int alpha = 255;
			if (pass == 1) {
				glDepthMask(false);
				glDepthFunc(GL_GREATER);
				alpha = 20;
			}

			t.begin(GL_QUADS);
			t.color(color, alpha);
			renderHoveredSide(t);
			t.end();
		}

		glDepthMask(true);
		glDisable(GL_BLEND);
		glDepthFunc(GL_LEQUAL);
	}

	private void renderHoveredSide(Tessellator t) {
		for (int i = 0; i < 6; i++) {
			if (selectedSide != i) continue;

			Vec3[] vertex = pickSides[i].points;
			int pp = 0;
			t.quad(vertex[pp++], vertex[pp++], vertex[pp++], vertex[pp++]);
		}
	}

	public void updateShape() {
		float x0 = bb.x;
		float y0 = bb.y;
		float z0 = bb.z;

		float x1 = bb.x + bb.xs;
		float y1 = bb.y + bb.ys;
		float z1 = bb.z + bb.zs;

		corners[0] = vc0 = new Vec3(x0, y0, z0);
		corners[1] = vc1 = new Vec3(x1, y0, z0);
		corners[2] = vc2 = new Vec3(x1, y0, z1);
		corners[3] = vc3 = new Vec3(x0, y0, z1);

		corners[4] = uc0 = new Vec3(x0, y1, z0);
		corners[5] = uc1 = new Vec3(x1, y1, z0);
		corners[6] = uc2 = new Vec3(x1, y1, z1);
		corners[7] = uc3 = new Vec3(x0, y1, z1);

		setPolygon(SIDE_UP, uc3, uc2, uc1, uc0);
		setPolygon(SIDE_DOWN, vc0, vc1, vc2, vc3);
		setPolygon(SIDE_NORTH, uc0, uc1, vc1, vc0);

		setPolygon(SIDE_EAST, uc1, uc2, vc2, vc1);
		setPolygon(SIDE_SOUTH, uc2, uc3, vc3, vc2);
		setPolygon(SIDE_WEST, uc3, uc0, vc0, vc3);

		pickables.clear();
		collidables.clear();
		polygons.clear();

		for (int i = 0; i < 6; i++) {
			pickables.add(pickSides[i]);
		}

		if (noCorner[0]) vc0 = null;
		if (noCorner[1]) vc1 = null;
		if (noCorner[2]) vc2 = null;
		if (noCorner[3]) vc3 = null;

		if (noCorner[4]) uc0 = null;
		if (noCorner[5]) uc1 = null;
		if (noCorner[6]) uc2 = null;
		if (noCorner[7]) uc3 = null;

		for (int i = 0; i < 8; i++) {
			int side = i / 4 * 2 - 1;
			int left = ((i - side) & 3) + (i & 4);
			int right = ((i + side) & 3) + (i & 4);
			int twoRight = ((i + side * 2) & 3) + (i & 4);
			int down = i ^ 4;
			int leftDown = left ^ 4;
			int rightDown = right ^ 4;
			int twoRightDown = twoRight ^ 4;

			if (noCorner[i] && noCorner[left] && noCorner[right] && noCorner[twoRight]) {
			} else if (noCorner[i] && noCorner[down] && noCorner[right] && noCorner[rightDown]) {
			} else if (noCorner[i] && !noCorner[left]) {
				if (tryAddPolygon(left, right, down)) {
				} else if (noCorner[right] && tryAddPolygon(left, twoRight, rightDown, down)) {
				} else if (noCorner[right] && noCorner[twoRight] && tryAddPolygon(left, rightDown, down, left, twoRightDown, rightDown)) {
				} else if (noCorner[down] && tryAddPolygon(left, right, rightDown, leftDown)) {
				} else if (noCorner[right] && noCorner[down] && tryAddPolygon(left, rightDown, leftDown, left, twoRight, rightDown)) {
				} else if (noCorner[right] && noCorner[rightDown] && tryAddPolygon(left, twoRight, down, twoRight, twoRightDown, down)) {
				} else if (noCorner[right] && noCorner[twoRight] && noCorner[rightDown] && tryAddPolygon(left, twoRightDown, down)) {
				} else {
					System.out.println("fix this!");
				}
			}
		}

		addPolygon(uc3, uc2, uc1, uc0);
		addPolygon(vc0, vc1, vc2, vc3);
		addPolygon(uc0, uc1, vc1, vc0);

		addPolygon(uc1, uc2, vc2, vc1);
		addPolygon(uc2, uc3, vc3, vc2);
		addPolygon(uc3, uc0, vc0, vc3);

		if (polygons.size() < 2) {
			ship.removeSelected();
		}

		ship.setDirty();
	}

	private void setPolygon(int side, Vec3 p0, Vec3 p1, Vec3 p2, Vec3 p3) {
		if (side < 0 || side >= pickSides.length) return;
		pickSides[side] = new PickablePolygon(side, p0, p1, p2, p3);
	}

	private boolean tryAddPolygon(int i0, int i1, int i2) {
		if (noCorner[i0] || noCorner[i1] || noCorner[i2]) return false;
		addPolygon(corners[i0], corners[i1], corners[i2], null);
		return true;
	}

	private boolean tryAddPolygon(int i0, int i1, int i2, int i3) {
		if (noCorner[i0] || noCorner[i1] || noCorner[i2] || noCorner[i3]) return false;
		addPolygon(corners[i0], corners[i1], corners[i2], corners[i3]);
		return true;
	}

	private boolean tryAddPolygon(int i0, int i1, int i2, int j0, int j1, int j2) {
		if (noCorner[i0] || noCorner[i1] || noCorner[i2]) return false;
		if (noCorner[j0] || noCorner[j1] || noCorner[j2]) return false;
		addPolygon(corners[i0], corners[i1], corners[i2], null);
		addPolygon(corners[j0], corners[j1], corners[j2], null);
		return true;
	}

	private boolean addPolygon(Vec3 p0, Vec3 p1, Vec3 p2, Vec3 p3) {
		Vec3[] tmp = new Vec3[4];
		int pp = 0;
		if (p0 != null) tmp[pp++] = p0;
		if (p1 != null) tmp[pp++] = p1;
		if (p2 != null) tmp[pp++] = p2;
		if (p3 != null) tmp[pp++] = p3;

		CollidablePolygon poly = null;
		if (pp == 3) {
			poly = new CollidablePolygon(tmp[0], tmp[1], tmp[2]);
			getLine(tmp[0], tmp[1]);
			getLine(tmp[1], tmp[2]);
			getLine(tmp[2], tmp[0]);
		} else if (pp == 4) {
			poly = new CollidablePolygon(tmp[0], tmp[1], tmp[2], tmp[3]);
			getLine(tmp[0], tmp[1]);
			getLine(tmp[1], tmp[2]);
			getLine(tmp[2], tmp[3]);
			getLine(tmp[3], tmp[0]);
		}

		if (poly != null) {
			polygons.add(poly);
			collidables.add(poly);
			return true;
		}
		return false;
	}

	private void getLine(Vec3 p0, Vec3 p1) {
		if (p0 == null || p1 == null) return;
		collidables.add(new CollidableLine(p0, p1));
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.addAll(this.collidables);
	}

	public void addPickables(List<Collidable> pickables) {
		pickables.addAll(this.pickables);
	}

	public byte getCornerMask() {
		int mask = 0;
		for (int i = 0; i < 8; i++) {
			if (noCorner[i]) mask |= (1 << i);
		}
		return (byte) mask;
	}

	public void setCornerMask(int mask) {
		for (int i = 0; i < 8; i++) {
			noCorner[i] = (mask &= (1 << i)) > 0;
		}
		updateShape();
	}

	public int hashCode() {
		int hashCode = 0;
		for (int i = 0; i < 8; i++) {
			hashCode |= corners[i].hashCode();
		}
		return hashCode;
	}

	public ShipPolygon clone(Vec3 pos) {
		ShipPolygon poly = new ShipPolygon(ship, pos.x, pos.y, pos.z, bb.xs, bb.ys, bb.zs, color);
		poly.setCornerMask(getCornerMask());
		return poly;
	}
}