package com.ixion.client.ship;

import java.io.*;
import java.util.*;

import vecmath.*;

import com.ixion.client.*;
import com.ixion.client.phys.*;
import com.ixion.client.ship.element.*;

public class Ship {
	public static String mainSave = "doc/saved/ship.dat";

	public AABB bb;
	private List<Collidable> collidables = new ArrayList<Collidable>();
	private List<Collidable> pickables = new ArrayList<Collidable>();
	private Random random = new Random();
	private boolean collidersDirty = true;
	private boolean visualsDirty = true;

	private List<ShipElement> elements = new ArrayList<ShipElement>();
	public ShipElement currentSelected, lastSelected, copied;
	public ShipBody body;

	public Ship(AABB bb) {
		this.bb = bb;
		body = new ShipBody(this);
	}

	public void move(Vec3 m) {
		bb.x += m.x;
		bb.y += m.y;
		bb.z += m.z;

		for (ShipElement element : elements) {
			element.move(m);
		}
		setDirty();
	}

	public void addNear(Vec3 pos) {
		for (int i = 0; i < 5; i++) {
			float r = (float) (Math.sqrt(i) + i / 2f);

			float xs = 0.7f;
			float ys = 0.7f;
			float zs = 0.7f;

			float x = pos.x + (random.nextFloat() * r) - (random.nextFloat() * r) - xs / 2f;
			float y = pos.y + (random.nextFloat() * r) - (random.nextFloat() * r) - ys / 2f;
			float z = pos.z + (random.nextFloat() * r) - (random.nextFloat() * r) - zs / 2f;
			ShipPolygon element = new ShipPolygon(this, x, y, z, 0.75f, 0.75f, 0.75f, random.nextInt());
			if (isFree(element.bb, null)) {
				add(element);
				return;
			}
		}
	}

	public void add(ShipElement element) {
		elements.add(element);
		setDirty();
	}

	public void removeSelected() {
		elements.remove(lastSelected);
		lastSelected = null;
		setDirty();
	}

	public void copySelected() {
		copied = lastSelected;
	}

	public void pasteSelected(Vec3 pos) {
		if (copied instanceof ShipPolygon) {
			ShipPolygon sp = (ShipPolygon) copied;
			ShipPolygon newPolygon = sp.clone(pos);

			if (isFree(newPolygon.bb, null)) {
				add(newPolygon);
			}
		}
	}

	public void tick(double delta) {
		for (ShipElement element : elements) {
			element.tick(delta);
		}
		setCollidersDirty(true);
	}

	public void render(FrustumCuller culler) {
		for (ShipElement element : elements) {
			AABB bb = element.bb;
			if (!culler.cubeInFrustum(bb.x, bb.y, bb.z, bb.xs, bb.ys, bb.zs)) continue;
			element.render();
		}
		body.render(null);
	}

	public void renderSelectedEffect() {
		if (currentSelected != null) {
			currentSelected.renderSelectedEffect();
		}

		lastSelected = currentSelected;
		currentSelected = null;
	}

	public boolean contains(AABB otherBB) {
		return bb.contains(otherBB);
	}

	public boolean isFree(AABB bb, ShipElement exception) {
		if (!contains(bb)) return false;

		for (ShipElement element : elements) {
			if (element == exception) continue;
			if (element.bb.overlaps(bb)) return false;
		}
		return true;
	}

	public List<Collidable> getCollidables() {
		checkCollidersDirty();
		return collidables;
	}

	public List<Collidable> getPickables() {
		checkCollidersDirty();
		return pickables;
	}

	private void checkCollidersDirty() {
		if (!collidersDirty) return;
		collidersDirty = false;

		collidables.clear();
		collidables.addAll(body.getCollidables());
		for (ShipElement element : elements) {
			element.addCollidables(collidables);
		}
		Collidable.removeDuplicates(collidables);

		pickables.clear();
		for (ShipElement element : elements) {
			element.addPickables(pickables);
		}
		Collidable.removeDuplicates(pickables);
	}

	public float getPowerDrainage() {
		float drainage = 0;
		for (ShipElement element : elements) {
			drainage += element.getPowerDrainage();
		}
		return drainage;
	}

	public int getOxygen() {
		return 100;
	}

	public void setDirty() {
		setVisualsDirty(true);
		setCollidersDirty(true);
	}

	public void setVisualsDirty(boolean b) {
		visualsDirty = b;
	}

	public boolean isVisualsDirty() {
		return visualsDirty;
	}

	public void setCollidersDirty(boolean b) {
		collidersDirty = b;
	}

	public boolean isCollidersDirty() {
		return collidersDirty;
	}

	public void save(String path) {
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(path)));

			dos.writeByte(1);
			dos.writeFloat(bb.x);
			dos.writeFloat(bb.y);
			dos.writeFloat(bb.z);

			dos.writeFloat(bb.xs);
			dos.writeFloat(bb.ys);
			dos.writeFloat(bb.zs);

			for (ShipElement se : elements) {
				if (se instanceof ShipPolygon) {
					ShipPolygon sp = (ShipPolygon) se;
					dos.writeByte(2);

					dos.writeFloat(sp.bb.x);
					dos.writeFloat(sp.bb.y);
					dos.writeFloat(sp.bb.z);

					dos.writeFloat(sp.bb.xs);
					dos.writeFloat(sp.bb.ys);
					dos.writeFloat(sp.bb.zs);

					dos.writeByte(sp.getCornerMask());
				}
			}
			dos.writeByte(0);
			dos.close();
		} catch (Exception e) {
			System.err.println("Error saving the ship!");
			e.printStackTrace();
		}
	}

	public static Ship load(String path) {
		try {
			DataInputStream dis = new DataInputStream(new FileInputStream(new File(path)));

			Ship ship = null;
			byte block = 0;
			while ((block = dis.readByte()) > 0) {
				if (block == 1) {
					float x = dis.readFloat();
					float y = dis.readFloat();
					float z = dis.readFloat();

					float xs = dis.readFloat();
					float ys = dis.readFloat();
					float zs = dis.readFloat();
					ship = new Ship(new AABB(x, y, z, xs, ys, zs));
				}

				if (block == 2) {
					float x = dis.readFloat();
					float y = dis.readFloat();
					float z = dis.readFloat();

					float xs = dis.readFloat();
					float ys = dis.readFloat();
					float zs = dis.readFloat();

					byte mask = dis.readByte();

					ShipPolygon sp = new ShipPolygon(ship, x, y, z, xs, ys, zs, 0xffffff);
					sp.setCornerMask(mask);
					ship.add(sp);
				}
			}
			dis.close();
			return ship;
		} catch (Exception e) {
			System.err.println("Error loading the ship!");
			e.printStackTrace();
		}
		return null;
	}
}