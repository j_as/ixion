package com.ixion.client.planet;

import vecmath.Vec3;

import com.ixion.client.renderer.Tessellator;

public class EdgeTriangle {
	public Edge edge;
	public int color;

	private Vec3 p0, p1, p2;

	public EdgeTriangle(Planet planet, Edge edge, int color) {
		this.edge = edge;
		this.color = color;

		int pp = 0;
		Vec3[] vertices = edge.getVertices(planet);
		p0 = vertices[pp++];
		p1 = vertices[pp++];
		p2 = vertices[pp++];
	}

	public void render(Tessellator t) {
		t.color(color);
		t.triangle(p0, p1, p2);
	}
}