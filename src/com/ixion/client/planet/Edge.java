package com.ixion.client.planet;

import java.util.List;

import vecmath.Vec3;

public class Edge {
	public int p0, p1, p2;
	private Vec3[] tmpVertices = new Vec3[3];

	public Edge(int p0, int p1, int p2) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
	}

	public Vec3[] getVertices(Planet planet) {
		int pp = 0;
		List<Vec3> vertices = planet.vertices;

		tmpVertices[pp++] = vertices.get(p0);
		tmpVertices[pp++] = vertices.get(p1);
		tmpVertices[pp++] = vertices.get(p2);
		return tmpVertices;
	}
}