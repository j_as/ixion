package com.ixion.client.planet;

import vecmath.Vec3;

public class Star extends Planet {
	public Star(Vec3 pos, float radius) {
		super(pos, radius);
	}

	protected int getTriangleColor() {
		int cc = random.nextInt(0xff0000);
		int br = 1;
		int r = cc / br;
		int g = cc / br;
		int b = cc / br;
		return r << 16 | g << 8 | b;
	}
}