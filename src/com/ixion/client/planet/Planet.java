package com.ixion.client.planet;

import static org.lwjgl.opengl.GL11.*;

import java.util.*;

import vecmath.*;

import com.ixion.client.FrustumCuller;
import com.ixion.client.renderer.Tessellator;

public class Planet {
	public static final float PHI = 1.61803398875f;
	public static final Random random = new Random();

	public List<Vec3> vertices = new ArrayList<Vec3>();
	public List<EdgeTriangle> triangles = new ArrayList<EdgeTriangle>();
	public Vec3 pos;
	public float radius;

	public Planet(Vec3 pos, float radius) {
		this.pos = pos;
		this.radius = radius;

		setup(new Vec3(-1f, PHI, 0), new Vec3(1f, PHI, 0), new Vec3(-1f, -PHI, 0), new Vec3(1f, -PHI, 0), new Vec3(0, -1f, PHI), new Vec3(0, 1f, PHI), new Vec3(0, -1f, -PHI), new Vec3(0, 1f, -PHI), new Vec3(PHI, 0, -1f), new Vec3(PHI, 0, 1f), new Vec3(-PHI, 0, -1f), new Vec3(-PHI, 0, 1f));
	}

	private void setup(Vec3 vx0, Vec3 vx1, Vec3 vx2, Vec3 vx3, Vec3 vy0, Vec3 vy1, Vec3 vy2, Vec3 vy3, Vec3 vz0, Vec3 vz1, Vec3 vz2, Vec3 vz3) {
		vertices.add(vx0);
		vertices.add(vx1);
		vertices.add(vx2);
		vertices.add(vx3);

		vertices.add(vy0);
		vertices.add(vy1);
		vertices.add(vy2);
		vertices.add(vy3);

		vertices.add(vz0);
		vertices.add(vz1);
		vertices.add(vz2);
		vertices.add(vz3);

		triangles.add(createTriangle(new Edge(0, 11, 5)));
		triangles.add(createTriangle(new Edge(0, 5, 1)));
		triangles.add(createTriangle(new Edge(0, 1, 7)));
		triangles.add(createTriangle(new Edge(0, 7, 10)));
		triangles.add(createTriangle(new Edge(0, 10, 11)));

		triangles.add(createTriangle(new Edge(1, 5, 9)));
		triangles.add(createTriangle(new Edge(5, 11, 4)));
		triangles.add(createTriangle(new Edge(11, 10, 2)));
		triangles.add(createTriangle(new Edge(10, 7, 6)));
		triangles.add(createTriangle(new Edge(7, 1, 8)));

		triangles.add(createTriangle(new Edge(3, 9, 4)));
		triangles.add(createTriangle(new Edge(3, 4, 2)));
		triangles.add(createTriangle(new Edge(3, 2, 6)));
		triangles.add(createTriangle(new Edge(3, 6, 8)));
		triangles.add(createTriangle(new Edge(3, 8, 9)));

		triangles.add(createTriangle(new Edge(4, 9, 5)));
		triangles.add(createTriangle(new Edge(2, 4, 11)));
		triangles.add(createTriangle(new Edge(6, 2, 10)));
		triangles.add(createTriangle(new Edge(8, 6, 7)));
		triangles.add(createTriangle(new Edge(9, 8, 1)));

		for (int i = 0; i < 5; i++) {
			subdivide(i);
		}

		for (Vec3 vertex : vertices) {
			vertex.normalise().mul(radius).add(pos);
		}
	}

	private EdgeTriangle createTriangle(Edge edge) {
		return new EdgeTriangle(this, edge, getTriangleColor());
	}

	public void subdivide(int level) {
		List<EdgeTriangle> newTriangles = new ArrayList<EdgeTriangle>();
		Hashtable<Long, Integer> midPointCache = new Hashtable<Long, Integer>(triangles.size() * 3, 0.8f);

		for (int i = 0; i < triangles.size(); i++) {
			EdgeTriangle t = triangles.get(i);

			int midA = getMidIndex(midPointCache, t.edge.p0, t.edge.p1);
			int midB = getMidIndex(midPointCache, t.edge.p1, t.edge.p2);
			int midC = getMidIndex(midPointCache, t.edge.p2, t.edge.p0);

			newTriangles.add(createTriangle(new Edge(t.edge.p0, midA, midC)));
			newTriangles.add(createTriangle(new Edge(t.edge.p1, midB, midA)));
			newTriangles.add(createTriangle(new Edge(t.edge.p2, midC, midB)));
			newTriangles.add(createTriangle(new Edge(midA, midB, midC)));
		}

		triangles = newTriangles;
	}

	private int getMidIndex(Hashtable<Long, Integer> midPointsCache, int v0, int v1) {
		boolean firstIsSmaller = v0 < v1;
		long l0 = firstIsSmaller ? v0 : v1;
		long l1 = firstIsSmaller ? v1 : v0;
		long key = (l0 << 32) + l1;
		if (midPointsCache.containsKey(key)) return midPointsCache.get(key);

		Vec3 t0 = vertices.get(v0);
		Vec3 t1 = vertices.get(v1);
		Vec3 mid = t0.clone().mid(t1).normalise();

		vertices.add(mid);
		int index = vertices.size() - 1;
		midPointsCache.put(key, index);
		return index;
	}

	public void render(FrustumCuller culler) {
		Tessellator t = Tessellator.instance;
		t.begin(GL_TRIANGLES);
		for (int i = 0; i < triangles.size(); i++) {
			EdgeTriangle et = triangles.get(i);

			if (isTopLevel(et, culler)) {
				et.render(t);
			}
		}
		t.end();
	}

	private boolean isTopLevel(EdgeTriangle et, FrustumCuller culler) {
		if (culler == null) return true;

		int pp = 0;
		Vec3[] vert = et.edge.getVertices(this);
		Vec3 p0 = vert[pp++];
		Vec3 p1 = vert[pp++];
		Vec3 p2 = vert[pp++];

		boolean b0 = culler.pointInFrustum(p0);
		boolean b1 = culler.pointInFrustum(p1);
		boolean b2 = culler.pointInFrustum(p2);

		return b0 && b1 && b2;
	}

	protected int getTriangleColor() {
		if (random.nextInt(6) <= 2) {
			int r = random.nextInt(random.nextInt(0x80) + 1) + 0x40;
			int g = random.nextInt(0x80) + 0x80;
			int b = random.nextInt(0x40);
			return r << 16 | g << 8 | b;
		}
		return 0x2040a0;
	}
}