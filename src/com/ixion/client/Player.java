package com.ixion.client;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.Ixion;
import com.ixion.client.phys.*;

public class Player {
	public static final float EYE_DISTANCE_FROM_TOP_OF_HEAD = 0.2f;
	public static final boolean NOCLIP = false;

	public MovableSphere upperBody;
	public MovableSphere lowerBody;
	public float maxSphereDist;
	public float minSphereDist;
	public float desiredSphereDist;
	public float radius = 0.5f;
	public float height = 1.80f;
	public float eyeDist;
	public boolean jump;
	public boolean onGround = false;
	private float jumpVelocity = -8f / 60f;
	private Collidable footCollider;

	public int health = 100;

	public Player(Vec3 pos) {
		eyeDist = radius - EYE_DISTANCE_FROM_TOP_OF_HEAD;
		desiredSphereDist = height - radius * 2;
		maxSphereDist = desiredSphereDist + 0.2f;
		minSphereDist = radius * 2;

		upperBody = new MovableSphere(new Vec3(pos.x, pos.y + height - radius, pos.z), radius);
		lowerBody = new MovableSphere(new Vec3(pos.x, pos.y + radius, pos.z), radius);
	}

	public void move(Vec3 motion) {
		upperBody.motion.add(motion);
		lowerBody.motion.add(motion);
	}

	public void physicsTick(List<Collidable> collidables, double delta) {
		Vec3 gravity = Ixion.gravityDir.clone().mul(Ixion.gravityPower);
		Vec3 gravityDir = gravity.clone().normalise();

		if (NOCLIP) {
			gravity.mul(0f);
			collidables.clear();
		}

		footCollider = lowerBody.intersect(gravity, collidables);

		onGround = true;
		if (footCollider == null) {
			onGround = false;
		}

		if (jump && onGround) {
			move(gravityDir.clone().mul(jumpVelocity));
			onGround = false;
			jump = false;
		}

		float friction = 1f;
		if (onGround) friction = 0.8f;

		Vec3 ol = lowerBody.pos.clone();
		lowerBody.motion.add(gravity);
		lowerBody.tryMove(lowerBody.motion.clone(), collidables);
		lowerBody.tryMove(gravity.clone(), collidables);
		lowerBody.motion.set(lowerBody.pos.clone().sub(ol));
		lowerBody.motion.mul(friction);

		Vec3 ou = upperBody.pos.clone();
		Vec3 desiredUpperBodyPos = lowerBody.pos.clone().add(gravityDir.clone().mul(-desiredSphereDist));
		Vec3 dist = desiredUpperBodyPos.sub(upperBody.pos.clone());
		upperBody.motion.lerp(dist, 0.5f);
		upperBody.tryMove(upperBody.motion, collidables);

		float dd = lowerBody.pos.distanceTo(upperBody.pos);
		if (dd == 0f) {
		} else {
			if (dd < minSphereDist) {
				float distToMove = dd - minSphereDist;
				Vec3 dir = upperBody.pos.clone().sub(lowerBody.pos).mul(-distToMove / dd);
				upperBody.tryMove(dir.clone().mul(0.7f), collidables);
				lowerBody.tryMove(dir.clone().mul(-0.2f), collidables);
			} else if (dd > maxSphereDist) {
				float distToMove = dd - maxSphereDist;
				Vec3 dir = upperBody.pos.clone().sub(lowerBody.pos).mul(-distToMove / dd);
				upperBody.tryMove(dir.clone().mul(0.7f), collidables);
				lowerBody.tryMove(dir.clone().mul(-0.2f), collidables);
			}
		}

		upperBody.motion.set(upperBody.pos.clone().sub(ou));
		upperBody.motion.mul(0.5f);
	}

	public Vec3 getEyePos() {
		return upperBody.pos.clone().add(0, EYE_DISTANCE_FROM_TOP_OF_HEAD, 0);
	}

	public Vec3 getFootPos() {
		return lowerBody.pos.clone();
	}

	public float getSpeed() {
		return lowerBody.motion.length() * 60f;
	}
}