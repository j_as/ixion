package com.ixion.client.game;

import static org.lwjgl.opengl.GL11.*;

import java.util.*;

import org.lwjgl.input.*;

import vecmath.*;

import com.ixion.client.*;
import com.ixion.client.menu.PauseScreen;
import com.ixion.client.phys.*;
import com.ixion.client.planet.*;
import com.ixion.client.renderer.*;
import com.ixion.client.ship.Ship;

import computer.*;

public class GameScreen extends Screen {
	private boolean buildMode = true;

	public int sceneList, hudList;
	public List<Light> lights = new ArrayList<Light>();
	public float xRot, yRot;
	private Vec3 forward1 = new Vec3(0, 0, 1f);
	private Vec3 right1 = new Vec3(1f, 0, 0);
	private FrustumCuller culler = new FrustumCuller();

	private AABB roomBB = new AABB(0f, 0f, 0f, 15f, 10f, 15f);
	public Ship ship = new Ship(roomBB);
	public Player player = new Player(new Vec3(5, 5, 5));

	public Mainframe cpu = new Mainframe();
	private VirtualMonitor vmonitor = (VirtualMonitor) new VirtualMonitor(new Vec3(-3.5f, 1f, 12f - 0.1f)).connectTo(cpu, 0x0);
	private VirtualKeyboard vkeyboard = (VirtualKeyboard) new VirtualKeyboard().connectTo(cpu, 0x1);
	private VirtualGenerator vgenerator = (VirtualGenerator) new VirtualGenerator().connectTo(cpu, 0x2);

	private Collidable dragCollidable;
	private float pickLength = 5f;
	private boolean behindToggle = false;
	private int dragButton = 0;

	private Planet planet = new Planet(new Vec3(-200, 0, 30), 120);
	private Moon moon = new Moon(new Vec3(-400, 0, 30), 70);

	public GameScreen() {
		Ship sp = Ship.load(Ship.mainSave);
		if (sp != null) {
			ship = sp;
		}

		long seed = new Random().nextLong();
		// seed = -7044415979476597446L;
		System.out.println("Lighting seed: " + seed);
		Random random = new Random(seed);
		lights.clear();
		lights.add(vmonitor.getLight());

		for (int i = 0; i < 3; i++) {
			float x = 1f + roomBB.x + (random.nextFloat() * roomBB.xs - 1f);
			float y = 1f + roomBB.y + (random.nextFloat() * roomBB.ys - 1f);
			float z = 1f + roomBB.z + (random.nextFloat() * roomBB.zs - 1f);

			float r = random.nextFloat() * 0.4f + 0.6f;
			float g = random.nextFloat() * 0.4f + 0.6f;
			float b = random.nextFloat() * 0.4f + 0.6f;
			Light light = new Light(x, y, z, r, g, b);

			if (i == 2) {
				light.ar = 0.15f;
				light.ag = 0.15f;
				light.ab = 0.15f;
			}

			lights.add(light);
		}

		/* lights.add(new Light(60f, 20f, 7.5f, 10f, 5f, 5f));
		 lights.add(new Light(50f, -10f, 7.5f, 5f, 5f, 10f));
		 lights.add(new Light(15f, 20f, 7.5f, 5f, 10f, 5f));
		 lights.add(new Light(-15f, 20f, 7.5f, 10f, 10f, 5f));*/
	}

	public void init(Ixion ixion) {
		super.init(ixion);
		cpu.init();

		sceneList = glGenLists(1);
		hudList = glGenLists(1);
	}

	public void tick(double delta) {
		if (!hasFocus) {
			setScreen(new PauseScreen(this));
			return;
		}

		cpu.tick(delta);
		ship.tick(delta);

		if (Mouse.isGrabbed()) {
			float dx = Mouse.getDX() * 0.05f;
			float dy = Mouse.getDY() * -0.05f;

			float rad = 22f / 7f / 180f;
			xRot += dy * rad;
			yRot += dx * rad;

			float viewRange = 70f * rad;
			if (xRot > viewRange) xRot = viewRange;
			if (xRot < -viewRange) xRot = -viewRange;
		}

		float walkSpeed = 0.03f;
		float runSpeed = 0.06f;
		float speed = walkSpeed;

		int xa = 0, za = 0;
		if (Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP)) za--;
		if (Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN)) za++;
		if (Keyboard.isKeyDown(Keyboard.KEY_A) || Keyboard.isKeyDown(Keyboard.KEY_LEFT)) xa--;
		if (Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) xa++;
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) speed = runSpeed;

		if (!player.onGround) {
			speed *= 0.1;
		}

		if (xa != 0 || za != 0) {
			Vec3 m = forward1.clone().mul(za).add(right1.clone().mul(xa)).normalise().mul(speed);
			player.move(m);
		}
		player.physicsTick(getCollidables(), delta);

		if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {
			if (zoomFactor < 2f) zoomFactor += 0.1f;
		} else {
			if (zoomFactor > 1f) zoomFactor -= 0.1f;
		}

		PickResult pr = pick();
		if (pr != null) {
			pr.collidable.onHovered(pr.pos);
		}

		if (dragCollidable != null) {
			Vec3 pos = getPickPos(2);
			dragCollidable.drag(pos);

			if (!Mouse.isButtonDown(dragButton)) {
				dragCollidable.stopDrag();
			}
		}

		float xx = 0, zz = 0;
		if (Keyboard.isKeyDown(Keyboard.KEY_I)) xx -= 0.1f;
		if (Keyboard.isKeyDown(Keyboard.KEY_K)) xx += 0.1f;
		if (Keyboard.isKeyDown(Keyboard.KEY_J)) zz += 0.1f;
		if (Keyboard.isKeyDown(Keyboard.KEY_L)) zz -= 0.1f;

		if (xx != 0 || zz != 0) {
			for (Light light : lights) {
				light.x += xx;
				// light.y += yy;
				light.z += zz;
			}

			player.lowerBody.pos.add(xx, 0, zz);

			ship.move(new Vec3(xx, 0, zz));
		}
	}

	private List<Collidable> getCollidables() {
		return ship.getCollidables();
	}

	private List<Collidable> getPickables() {
		return ship.getPickables();
	}

	private PickResult pick() {
		if (!buildMode) return null;

		Vec3 dir = forward1.clone().mul(-pickLength);
		Vec3 pos = player.getEyePos();

		Collidable c = null;
		MovableSphere ms = new MovableSphere(pos, 0);

		if (behindToggle) {
			pos.add(dir);
			dir.mul(-1f);
			c = ms.lastIntersection(dir, getPickables());
		} else {
			c = ms.intersect(dir, getPickables());
		}

		if (c != null) {
			return new PickResult(ms.pos, c);
		}
		return null;
	}

	private Vec3 getPickPos(float length) {
		return player.getEyePos().add(forward1.clone().mul(-length));
	}

	public void keyPressed(int key) {
		if (key == Keyboard.KEY_ESCAPE) {
			setScreen(new PauseScreen(this));
		} else if (key == Keyboard.KEY_1 && buildMode) {
			ship.addNear(getPickPos(pickLength));
		} else if (key == Keyboard.KEY_0 && buildMode) {
			ship.removeSelected();
		} else if (key == Keyboard.KEY_SPACE) {
			player.jump = true;
		} else if ((key == Keyboard.KEY_LCONTROL || key == Keyboard.KEY_RCONTROL) && buildMode) {
			behindToggle ^= true;
		} else if (key == Keyboard.KEY_C && buildMode) {
			ship.copySelected();
		} else if (key == Keyboard.KEY_V && buildMode) {
			ship.pasteSelected(getPickPos(pickLength));
		} else {
		}

		vkeyboard.wasPressed(Keyboard.getEventCharacter(), key);
	}

	public void keyReleased(int key) {
		vkeyboard.wasReleased(Keyboard.getEventCharacter(), key);
	}

	public void onClick(int mb) {
		if (mb == dragButton && buildMode) {
			PickResult pr = pick();
			dragCollidable = null;

			if (pr != null) {
				dragCollidable = pr.collidable;
				pr.collidable.onClick(mb, pr.pos);
				dragCollidable.startDrag(getPickPos(2), Mouse.isButtonDown(dragButton), Keyboard.isKeyDown(Keyboard.KEY_TAB));
			}
		}
	}

	public void renderScene() {
		if (ship.isVisualsDirty()) {
			ship.setVisualsDirty(false);

			glNewList(sceneList, GL_COMPILE);
			renderSceneList();
			glEndList();
		}

		viewMatrix.rotX(xRot);
		viewMatrix.rotY(yRot);
		viewMatrix.translate(player.getEyePos().mul(-1f));

		Mat4 normalMatrix = createNormalMatrix();

		forward1 = viewMatrix.mul(forward).normalise();
		right1 = viewMatrix.mul(right).normalise();
		culler.updatePlanes(projectionMatrix, viewMatrix);

		Shader.basicShader.enable();
		updateUniforms();
		planet.render(culler);
		moon.render(culler);
		Shader.basicShader.disable();

		modelMatrix.translate(ship.bb.getMin());

		Shader.lightShader.enable();
		updateUniforms();
		glEnable(GL_BLEND);
		for (int i = 0; i < lights.size(); i++) {
			Light light = lights.get(i);
			light.quadraticAttenuation = 1f / lights.size();
			light.updateUniforms(normalMatrix);

			if (i > 0) glBlendFunc(GL_ONE, GL_ONE);
			glCallList(sceneList);
		}
		glDisable(GL_BLEND);
		Shader.lightShader.disable();

		Shader.hoverShader.enable();
		updateUniforms();
		glDisable(GL_CULL_FACE);
		ship.renderSelectedEffect();
		glEnable(GL_CULL_FACE);
		Shader.hoverShader.disable();

		Shader.displayShader.enable();
		updateUniforms();
		vmonitor.render();
		Shader.displayShader.disable();
	}

	private void renderSceneList() {
		vmonitor.renderMonitorFrame();
		ship.render(culler);
	}

	public void renderHud() {
		glNewList(hudList, GL_COMPILE);
		Shader.basicShader.enable();
		updateUniforms();
		float xm = screenWidth / 2f;
		float ym = screenHeight / 2f;
		float zm = 0f;
		float s = 2f;
		float p = 5f;

		glDisable(GL_CULL_FACE);
		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);
		t.color(0);
		t.quad(xm - s, ym - s - p, zm, xm - s, ym + s + p, zm, xm + s, ym + s + p, zm, xm + s, ym - s - p, zm);
		t.quad(xm - s - p, ym - s, zm, xm - s - p, ym + s, zm, xm + s + p, ym + s, zm, xm + s + p, ym - s, zm);
		t.end();
		s = 1f;
		t.begin(GL_QUADS);
		t.color(0xffffff);
		t.quad(xm - s, ym - s - p, zm, xm - s, ym + s + p, zm, xm + s, ym + s + p, zm, xm + s, ym - s - p, zm);
		t.quad(xm - s - p, ym - s, zm, xm - s - p, ym + s, zm, xm + s + p, ym + s, zm, xm + s + p, ym - s, zm);
		t.end();
		glEnable(GL_CULL_FACE);
		Shader.basicShader.disable();

		/*		s = 0.5f;
				viewMatrix.scale(s, s, 1f);

				Shader.textureShader.enable();
				updateUniforms();
				renderer.texturedQuad(Texture.monitorTexture, 0, -36f / s);
				Shader.textureShader.disable();*/

		s = 100;
		int pp = 1;

		Shader.textureShader.enable();
		updateUniforms();
		super.renderHud();
		renderer.transparentQuad(10, 10, 140, 350, 0xffffff, 40);
		renderer.renderHudText(player.health + "%", "health", 25, pp++ * s - 50);
		renderer.renderHudText(vgenerator.getPower() + "%", "power", 25, pp++ * s - 50);
		renderer.renderHudText(ship.getOxygen() + "%", "oxygen", 25, pp++ * s - 50);
		Shader.textureShader.disable();
		glEndList();

		glCallList(hudList);
	}

	public boolean shouldGrabScreen() {
		return true;
	}
}