package com.ixion.client.shape;

import static org.lwjgl.opengl.GL11.GL_QUADS;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.Collidable;
import com.ixion.client.phys.CollidablePolygon;
import com.ixion.client.renderer.Tessellator;

public class Quadrilateral extends Shape {
	private Vec3 p0, p1, p2, p3;
	private CollidablePolygon collidable;

	public Quadrilateral(Vec3 p0, Vec3 p1, Vec3 p2, Vec3 p3) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		collidable = new CollidablePolygon(p0, p1, p2, p3);
	}

	public void render(FrustumCuller culler) {
		if (!isTopLevel(culler)) return;

		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);
		t.color(color);
		t.quad(p0, p1, p2, p3);
		t.end();
	}

	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		boolean b0 = culler.pointInFrustum(p0);
		boolean b1 = culler.pointInFrustum(p1);
		boolean b2 = culler.pointInFrustum(p2);
		boolean b3 = culler.pointInFrustum(p3);
		return b0 && b1 && b2 && b3;
	}

	public Vec3 getNormal() {
		Vec3 u = p1.clone().sub(p0);
		Vec3 v = p3.clone().sub(p0);
		return u.cross(v).normalise();
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.add(collidable);
	}
}