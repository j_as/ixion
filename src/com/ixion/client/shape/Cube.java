package com.ixion.client.shape;

import static org.lwjgl.opengl.GL11.GL_QUADS;

import java.util.ArrayList;
import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.AABB;
import com.ixion.client.phys.Collidable;
import com.ixion.client.phys.CollidableLine;
import com.ixion.client.phys.CollidablePolygon;
import com.ixion.client.renderer.Tessellator;

public class Cube extends Shape {
	private AABB bb = new AABB();

	private Vec3 uc0 = new Vec3();
	private Vec3 uc1 = new Vec3();
	private Vec3 uc2 = new Vec3();
	private Vec3 uc3 = new Vec3();

	private Vec3 vc0 = new Vec3();
	private Vec3 vc1 = new Vec3();
	private Vec3 vc2 = new Vec3();
	private Vec3 vc3 = new Vec3();

	private List<Collidable> collidables = new ArrayList<Collidable>();

	public Cube(float x, float y, float z, float xs, float ys, float zs) {
		bb.set(x, y, z, xs, ys, zs);

		vc0.set(x, y, z);
		vc1.set(x + xs, y, z);
		vc2.set(x + xs, y, z + zs);
		vc3.set(x, y, z + zs);

		uc0.set(x, y + ys, z);
		uc1.set(x + xs, y + ys, z);
		uc2.set(x + xs, y + ys, z + zs);
		uc3.set(x, y + ys, z + zs);

		collidables.add(new CollidablePolygon(uc3, uc2, uc1, uc0));
		collidables.add(new CollidablePolygon(vc0, vc1, vc2, vc3));
		collidables.add(new CollidablePolygon(uc0, uc1, vc1, vc0));
		collidables.add(new CollidablePolygon(uc1, uc2, vc2, vc1));
		collidables.add(new CollidablePolygon(uc2, uc3, vc3, vc2));
		collidables.add(new CollidablePolygon(uc3, uc0, vc0, vc3));

		collidables.add(new CollidableLine(uc0, uc1));
		collidables.add(new CollidableLine(uc1, uc2));
		collidables.add(new CollidableLine(uc2, uc3));
		collidables.add(new CollidableLine(uc3, uc0));

		collidables.add(new CollidableLine(vc0, vc1));
		collidables.add(new CollidableLine(vc1, vc2));
		collidables.add(new CollidableLine(vc2, vc3));
		collidables.add(new CollidableLine(vc3, vc0));

		collidables.add(new CollidableLine(uc0, vc0));
		collidables.add(new CollidableLine(uc1, vc1));
		collidables.add(new CollidableLine(uc2, vc2));
		collidables.add(new CollidableLine(uc3, vc3));
	}

	public void render(FrustumCuller culler) {
		if (!isTopLevel(culler)) return;

		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);
		t.color(color);
		t.quad(uc3, uc2, uc1, uc0);
		t.quad(vc0, vc1, vc2, vc3);
		t.quad(uc0, uc1, vc1, vc0);
		t.quad(uc1, uc2, vc2, vc1);
		t.quad(uc2, uc3, vc3, vc2);
		t.quad(uc3, uc0, vc0, vc3);
		t.end();
	}

	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		return culler.cubeInFrustum(bb.x, bb.y, bb.z, bb.xs, bb.ys, bb.zs);
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.addAll(this.collidables);
	}
}