package com.ixion.client.shape;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.Collidable;

public class Shape {
	public int color = 0xffffff;

	public void render(FrustumCuller culler) {
	}
	
	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		return false;
	}

	public Vec3 getNormal() {
		return null;
	}

	public void addCollidables(List<Collidable> collidables) {
	}
}