package com.ixion.client.shape;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.Collidable;
import com.ixion.client.phys.CollidablePolygon;
import com.ixion.client.renderer.Tessellator;

public class Triangle extends Shape {
	private Vec3 p0, p1, p2;
	private CollidablePolygon collidable;

	public Triangle(Vec3 p0, Vec3 p1, Vec3 p2) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		collidable = new CollidablePolygon(p0, p1, p2);
	}

	public void render(FrustumCuller culler) {
		if (!isTopLevel(culler)) return;

		Tessellator t = Tessellator.instance;
		t.begin(GL_TRIANGLES);
		t.color(color);
		t.triangle(p0, p1, p2);
		t.end();
	}

	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		boolean b0 = culler.pointInFrustum(p0);
		boolean b1 = culler.pointInFrustum(p1);
		boolean b2 = culler.pointInFrustum(p2);
		return b0 && b1 && b2;
	}

	public Vec3 getNormal() {
		Vec3 u = p1.clone().sub(p0);
		Vec3 v = p2.clone().sub(p0);
		return v.cross(u).normalise();
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.add(collidable);
	}
}