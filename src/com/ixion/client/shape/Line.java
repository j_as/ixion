package com.ixion.client.shape;

import static org.lwjgl.opengl.GL11.GL_LINES;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.Collidable;
import com.ixion.client.phys.CollidableLine;
import com.ixion.client.renderer.Tessellator;

public class Line extends Shape {
	private Vec3 p0, p1;
	private CollidableLine collidable;

	public Line(Vec3 p0, Vec3 p1) {
		this.p0 = p0;
		this.p1 = p1;
		collidable = new CollidableLine(p0, p1);
	}

	public void render(FrustumCuller culler) {
		if (!isTopLevel(culler)) return;

		Tessellator t = Tessellator.instance;
		t.begin(GL_LINES);
		t.color(color);
		t.line(p0, p1);
		t.end();
	}

	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		return culler.pointInFrustum(p0) && culler.pointInFrustum(p1);
	}

	public Vec3 getNormal() {
		return p0.clone().cross(p1).normalise();
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.add(collidable);
	}
}