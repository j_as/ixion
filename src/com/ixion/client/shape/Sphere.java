package com.ixion.client.shape;

import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_QUAD_STRIP;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.List;

import vecmath.Vec3;

import com.ixion.client.FrustumCuller;
import com.ixion.client.phys.Collidable;
import com.ixion.client.phys.CollidableSphere;
import com.ixion.client.renderer.Tessellator;

public class Sphere extends Shape {
	private Vec3 pos;
	private float radius;
	private CollidableSphere collidable;

	private int lats = 20;
	private int longs = 20;

	public Sphere(Vec3 pos, float radius) {
		this.pos = pos;
		this.radius = radius;
		collidable = new CollidableSphere(pos, radius);
	}

	public void render(FrustumCuller culler) {
		if (!isTopLevel(culler)) return;

		glDisable(GL_CULL_FACE);
		{
			Tessellator t = Tessellator.instance;
			t.begin(GL_QUAD_STRIP);
			t.color(color);
			t.sphere(pos, lats, longs, radius);
			t.end();
		}
		glEnable(GL_CULL_FACE);
	}

	public boolean isTopLevel(FrustumCuller culler) {
		if (culler == null) return true;
		return culler.sphereInFrustum(pos, radius);
	}

	public void addCollidables(List<Collidable> collidables) {
		collidables.add(collidable);
	}
}