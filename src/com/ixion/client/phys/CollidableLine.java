package com.ixion.client.phys;

import vecmath.Vec3;

public class CollidableLine extends Collidable {
	public Vec3 p0, p1;
	public Vec3 normal;
	private float d, len;

	private Vec3 v = new Vec3();
	private Vec3 c = new Vec3();
	private Vec3 dd = new Vec3();

	private CollidableSphere sphere = new CollidableSphere(new Vec3(0, 0, 0), 0);

	public CollidableLine(Vec3 p0, Vec3 p1) {
		this.p0 = p0;
		this.p1 = p1;

		normal = p1.clone().sub(p0).normalise();
		d = p0.dot(normal);
		len = p1.distanceTo(p0);
	}

	public Collision getCollision(MovableSphere ss, Vec3 s0, Vec3 s1) {
		dd.set(normal).cross(v.set(s0).sub(p0));
		v.set(s1).sub(s0);
		c.set(normal).cross(v);

		float r = ss.radius;
		float A = c.dot(c);
		float B = c.dot(dd) * 2;
		float C = dd.dot(dd) - r * r;

		float mid = B * B - 4 * A * C;
		if (A != 0 && mid <= 0) {
			mid = (float) Math.sqrt(mid);
			float t0 = (-B + mid) / (2 * A);
			float t1 = (-B - mid) / (2 * A);
			if (t0 <= 0) t0 = 1000;
			if (t1 <= 0) t1 = 1000;
			float t = t0 < t1 ? t0 : t1;

			if (t < 1) {
				Vec3 pos = c.set(s0).lerp(s1, t);

				float dd = pos.dot(normal) - d;
				if (dd < 0) dd = 0;
				if (dd > len) dd = len;

				Vec3 op = new Vec3(p0).mulAdd(normal, dd);
				if (dd == 0 || dd == len) {
					sphere.pos.set(op);
					return sphere.getCollision(ss, s0, s1);
				} else {
					Vec3 normal = pos.clone().sub(op).normalise();
					return new Collision(this, t, normal);
				}
			}
		} else {
			sphere.pos.set(p0);
			Collision c0 = sphere.getCollision(ss, s0, s1);

			sphere.pos.set(p1);
			Collision c1 = sphere.getCollision(ss, s0, s1);

			if (c0 == null && c1 == null) return null;
			if (c0 != null && c1 == null) return c0;
			if (c0 == null && c1 != null) return c1;
			if (c0.time < c1.time) return c0;
			return c1;
		}
		return null;
	}

	public float distanceTo(Vec3 pos) {
		float dd = pos.dot(normal) - d;
		sphere.pos.set(p0).mulAdd(normal, dd);
		return sphere.distanceTo(pos);
	}

	public float distanceToSqr(Vec3 pos) {
		float dd = pos.dot(normal) - d;
		sphere.pos.set(p0).mulAdd(normal, dd);
		return sphere.distanceToSqr(pos);
	}

	public void eject(MovableSphere ms, Vec3 pos) {
		float dd = pos.dot(normal) - d;
		if (dd < 0) dd = 0;
		if (dd > len) dd = len;

		sphere.pos.set(p0).mulAdd(normal, dd);
		sphere.eject(ms, pos);
	}

	public Intersection getIntersection(Vec3 pos, float r) {
		float dd = pos.dot(normal) - d;
		if (dd < 0) dd = 0;
		if (dd > len) dd = len;

		sphere.pos.set(p0).mulAdd(normal, dd);
		return sphere.getIntersection(pos, r);
	}

	public int hashCode() {
		return p0.hashCode() ^ p1.hashCode();
	}

	public boolean equals(Object o) {
		if (o instanceof CollidableLine) {
			CollidableLine line = (CollidableLine) o;
			if (line.p0.equals(p0) && line.p1.equals(p1)) return true;
			if (line.p1.equals(p0) && line.p0.equals(p1)) return true;
		}
		return false;
	}
}