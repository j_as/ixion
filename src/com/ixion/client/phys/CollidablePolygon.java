package com.ixion.client.phys;

import vecmath.Vec3;

public class CollidablePolygon extends CollidablePlane {
	private CollidablePlane[] sides;
	public Vec3[] points;

	public CollidablePolygon(Vec3... nodes) {
		super(nodes[0], nodes[1], nodes[2]);
		points = nodes;

		sides = new CollidablePlane[nodes.length];
		for (int i = 0; i < sides.length; i++) {
			sides[i] = new CollidablePlane(nodes[i], nodes[(i + 1) % nodes.length], nodes[i].clone().sub(normal));
		}
	}

	public Collision getCollision(MovableSphere sphere, Vec3 p0, Vec3 p1) {
		float before = p0.dot(normal) - d;
		float after = p1.dot(normal) - d;
		if (after >= before) return null;
		if (before > 0 && after < sphere.radius) {
			float time = (sphere.radius - before) / (after - before);
			Vec3 pos = p0.clone().lerp(p1, time);
			if (isInside(pos)) return new Collision(this, time, normal);
		}
		return null;
	}

	public void eject(MovableSphere sphere, Vec3 pos) {
		float diff = pos.dot(normal) - d;
		if (diff > 0 && diff < sphere.radius) {
			if (isInside(pos)) {
				float distToEject = sphere.radius - diff;
				pos.mulAdd(normal, distToEject);
			}
		}
	}

	public Intersection getIntersection(Vec3 pos, float r) {
		float diff = pos.dot(normal) - d;
		if (diff > 0 && diff < r) {
			if (isInside(pos)) return new Intersection(this, normal);
		}
		return null;
	}

	public boolean isInside(Vec3 pos) {
		for (int i = 0; i < sides.length; i++) {
			float dist = sides[i].distanceTo(pos);
			if (dist < 0) return false;
		}
		return true;
	}

	public int hashCode() {
		int hashCode = 0;
		for (int i = 0; i < points.length; i++) {
			hashCode ^= points[i].hashCode();
		}
		return hashCode;
	}

	public boolean equals(Object o) {
		if (o instanceof CollidablePolygon) {
			CollidablePolygon poly = (CollidablePolygon) o;
			if (poly.points.length != points.length) return false;

			findStartPoint: for (int i = 0; i < points.length; i++) {
				if (points[0].equals(poly.points[i])) {
					for (int j = 0; j < points.length; j++) {
						if (!points[j].equals(poly.points[(i + j) % points.length])) {
							continue findStartPoint;
						}
					}
					return true;
				}
			}
		}
		return false;
	}
}