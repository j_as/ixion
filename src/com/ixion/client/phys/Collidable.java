package com.ixion.client.phys;

import java.util.*;

import vecmath.Vec3;

public abstract class Collidable {
	public abstract Collision getCollision(MovableSphere ms, Vec3 p0, Vec3 p1);

	public abstract void eject(MovableSphere ms, Vec3 pos);

	public abstract Intersection getIntersection(Vec3 pos, float r);

	public abstract float distanceTo(Vec3 pos);

	public abstract float distanceToSqr(Vec3 pos);

	public static <T extends Collidable> void removeDuplicates(List<T> collidables) {
		Collection<T> uniqueCollidables = new HashSet<T>();
		uniqueCollidables.addAll(collidables);

		int removedDuplicates = collidables.size() - uniqueCollidables.size();
		if (removedDuplicates > 0) {
			collidables.clear();
			collidables.addAll(uniqueCollidables);
		}
	}

	public void onCollided() {
	}

	public void onHovered(Vec3 pos) {
	}

	public void onClick(int mb, Vec3 pos) {
	}

	public void startDrag(Vec3 startDragPos, boolean dragging, boolean tabDown) {
	}

	public void drag(Vec3 dragPos) {
	}

	public void stopDrag() {
	}
}