package com.ixion.client.phys;

import vecmath.Vec3;

public class Collision {
	public Collidable collidable;
	public float time;
	public Vec3 normal;

	public Collision(Collidable collidable, float time, Vec3 normal) {
		this.collidable = collidable;
		this.time = time;
		this.normal = normal;
	}
}