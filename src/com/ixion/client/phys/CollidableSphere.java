package com.ixion.client.phys;

import vecmath.Vec3;

public class CollidableSphere extends Collidable {
	public Vec3 pos;
	public float radius;

	public CollidableSphere(Vec3 pos, float radius) {
		this.pos = pos;
		this.radius = radius;
	}

	public Collision getCollision(MovableSphere sphere, Vec3 p0, Vec3 p1) {
		float r = radius + sphere.radius;
		Vec3 d = p1.clone().sub(p0).normalise();
		Vec3 m = p0.clone().sub(pos);
		float b = m.dot(d);
		float c = m.dot(m) - r * r;
		if (c > 0.0 && b > 0.0) return null;
		float discr = b * b - c;
		if (discr < 0) return null;
		float t = (float) (-b - Math.sqrt(discr));
		if (t < 0) return null;
		t /= p0.distanceTo(p1);
		if (t > 1) return null;

		Vec3 normal = p0.clone().lerp(p1, t).sub(pos).normalise().mul(-1);
		return new Collision(this, t, normal);
	}

	public void eject(MovableSphere sphere, Vec3 p) {
		float xd = p.x - pos.x;
		float yd = p.y - pos.y;
		float zd = p.z - pos.z;
		float dd = xd * xd + yd * yd + zd * zd;
		float rr = (radius + sphere.radius);

		if (dd < rr * rr) {
			dd = (float) Math.sqrt(dd);
			xd /= dd;
			yd /= dd;
			zd /= dd;

			float amount = (rr - dd);
			p.add(xd * amount, yd * amount, zd * amount);
		}
	}

	public Intersection getIntersection(Vec3 p, float r) {
		float xd = p.x - pos.x;
		float yd = p.y - pos.y;
		float zd = p.z - pos.z;
		float dd = xd * xd + yd * yd + zd * zd;
		float rr = (radius + r);
		if (rr < 0) rr = 0;
		if (dd > rr * rr) return null;
		return new Intersection(this, new Vec3(xd, yd, zd).normalise());
	}

	public float distanceTo(Vec3 p) {
		float xd = p.x - pos.x;
		float yd = p.y - pos.y;
		float zd = p.z - pos.z;
		return (float) Math.sqrt(xd * xd + yd * yd + zd * zd);
	}

	public float distanceToSqr(Vec3 p) {
		float xd = p.x - pos.x;
		float yd = p.y - pos.y;
		float zd = p.z - pos.z;
		return xd * xd + yd * yd * zd * zd;
	}

	public int hashCode() {
		return pos.hashCode() ^ ((int) (radius * 4239761));
	}

	public boolean equals(Object o) {
		if (o instanceof CollidableSphere) {
			CollidableSphere sphere = (CollidableSphere) o;
			if (sphere.pos.equals(pos) && radius == sphere.radius) return true;
		}
		return false;
	}

}