package com.ixion.client.phys;

import vecmath.Vec3;

public class CollidablePlane extends Collidable {
	public Vec3 normal;
	public float d;

	public CollidablePlane(Vec3 p0, Vec3 p1, Vec3 p2) {
		Vec3 np0 = p1.clone().sub(p0).normalise();
		Vec3 np1 = p2.clone().sub(p0).normalise();
		normal = np0.cross(np1).normalise();
		d = normal.dot(p0);
	}

	public Collision getCollision(MovableSphere sphere, Vec3 p0, Vec3 p1) {
		float before = p0.dot(normal) - d;
		float after = p1.dot(normal) - d;
		if (after >= before) return null;
		if (before > 0 && after < sphere.radius) {
			float time = (sphere.radius - before) / (after - before);
			return new Collision(this, time, normal);
		}
		return null;
	}

	public void eject(MovableSphere sphere, Vec3 pos) {
		float diff = pos.dot(normal) - d;
		if (diff > 0 && diff < sphere.radius) {
			float distToEject = sphere.radius - diff;
			pos.mulAdd(normal, distToEject);
		}
	}

	public Intersection getIntersection(Vec3 pos, float r) {
		float diff = pos.dot(normal) - d;
		if (diff > 0 && diff < r) return new Intersection(this, normal);
		return null;
	}

	public float distanceTo(Vec3 pos) {
		return pos.dot(normal) - d;
	}

	public float distanceToSqr(Vec3 pos) {
		float dd = pos.dot(normal) - d;
		return dd * dd;
	}
}