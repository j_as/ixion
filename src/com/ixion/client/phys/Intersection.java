package com.ixion.client.phys;

import vecmath.Vec3;

public class Intersection {
	public Collidable collidable;
	public Vec3 normal;

	public Intersection(Collidable collidable, Vec3 normal) {
		this.collidable = collidable;
		this.normal = normal;
	}
}