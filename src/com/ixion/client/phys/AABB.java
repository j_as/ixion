package com.ixion.client.phys;

import vecmath.Vec3;

public class AABB {
	public float x, y, z, xs, ys, zs;

	private Vec3 min = new Vec3();
	private Vec3 max = new Vec3();

	public AABB() {
		this(0, 0, 0, 0, 0, 0);
	}

	public AABB(float x, float y, float z, float xs, float ys, float zs) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.xs = xs;
		this.ys = ys;
		this.zs = zs;
	}

	public AABB(Vec3 min, Vec3 max) {
		this.min = min;
		this.max = max;
		
		x = min.x;
		y = min.y;
		z = min.z;
		xs = x + max.x;
		ys = y + max.y;
		zs = z + max.z;
	}

	public AABB(AABB bb) {
		x = bb.x;
		y = bb.y;
		z = bb.z;
		xs = bb.xs;
		ys = bb.ys;
		zs = bb.zs;
	}

	public void set(float x, float y, float z, float xs, float ys, float zs) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.xs = xs;
		this.ys = ys;
		this.zs = zs;
	}

	public void set(AABB bb) {
		x = bb.x;
		y = bb.y;
		z = bb.z;
		xs = bb.xs;
		ys = bb.ys;
		zs = bb.zs;
	}

	public boolean contains(AABB bb) {
		if (bb.x < x || bb.y < y || bb.z < z) return false;
		if (bb.x + bb.xs > x + xs || bb.y + bb.ys > y + ys || bb.z + bb.zs > z + zs) return false;
		return true;
	}

	public boolean overlaps(AABB bb) {
		if (bb.x + bb.xs < x || bb.y + bb.ys < y || bb.z + bb.zs < z) return false;
		if (bb.x > x + xs || bb.y > y + ys || bb.z > z + zs) return false;
		return true;
	}

	public Vec3 getMin() {
		return min.set(x, y, z);
	}

	public Vec3 getMax() {
		return max.set(x + xs, y + ys, z + zs);
	}

	public boolean equals(Object o) {
		if (o instanceof AABB) {
			AABB bb = (AABB) o;
			if (x == bb.x && y == bb.y && z == bb.z && xs == bb.xs && ys == bb.ys && zs == bb.zs) return true;
		}
		return false;
	}
}