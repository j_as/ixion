package com.ixion.client.phys;

import java.util.List;

import vecmath.Vec3;

public class MovableSphere extends CollidableSphere {
	private Vec3 nextPos = new Vec3(0, 0, 0);
	private Vec3 pp = new Vec3();

	public Vec3 motion = new Vec3(0, 0, 0);

	public MovableSphere(Vec3 pos, float radius) {
		super(pos, radius);
	}

	public void tryMove(Vec3 motion, List<Collidable> collidables) {
		float remainingTime = 1;
		int maxIterations = 10;

		while (remainingTime > 0 && maxIterations-- > 0) {
			nextPos.set(pos).mulAdd(motion, 1);
			Collision nearestCollision = null;
			for (int i = 0; i < collidables.size(); i++) {
				Collision c = collidables.get(i).getCollision(this, pos, nextPos);
				if (c == null) continue;
				if (c.time > remainingTime) continue;
				if (nearestCollision == null || nearestCollision.time > c.time) {
					nearestCollision = c;
				}
			}
			if (nearestCollision == null) {
				movePullback(pos, motion, remainingTime, collidables);
				remainingTime = 0;
			} else {
				nearestCollision.collidable.onCollided();
				if (nearestCollision.time < 0) nearestCollision.time = 0;
				movePullback(pos, motion, nearestCollision.time, collidables);
				float dd = motion.dot(nearestCollision.normal);
				motion.mulAdd(nearestCollision.normal, -dd * 1.01f);
				remainingTime -= nearestCollision.time;
			}
		}
	}

	public Collidable intersect(Vec3 motion, List<Collidable> collidables) {
		nextPos.set(pos).mulAdd(motion, 1);
		Collision nearestCollision = null;
		for (int i = 0; i < collidables.size(); i++) {
			Collision c = collidables.get(i).getCollision(this, pos, nextPos);
			if (c == null) continue;
			if (nearestCollision == null || nearestCollision.time > c.time) {
				nearestCollision = c;
			}
		}
		if (nearestCollision == null) {
			movePullback(pos, motion, 1, collidables);
		} else {
			nearestCollision.collidable.onCollided();
			if (nearestCollision.time < 0) nearestCollision.time = 0;
			movePullback(pos, motion, nearestCollision.time, collidables);
		}
		return nearestCollision == null ? null : nearestCollision.collidable;
	}

	public Collidable lastIntersection(Vec3 motion, List<Collidable> collidables) {
		nextPos.set(pos).mulAdd(motion, 1);
		Collision nearestCollision = null;
		for (int i = 0; i < collidables.size(); i++) {
			Collision c = collidables.get(i).getCollision(this, pos, nextPos);
			if (c == null) continue;
			if (nearestCollision == null || nearestCollision.time < c.time) {
				nearestCollision = c;
			}
		}
		if (nearestCollision == null) {
			movePullback(pos, motion, 1, collidables);
		} else {
			nearestCollision.collidable.onCollided();
			if (nearestCollision.time < 0) nearestCollision.time = 0;
			movePullback(pos, motion, nearestCollision.time, collidables);
		}
		return nearestCollision == null ? null : nearestCollision.collidable;
	}

	private void movePullback(Vec3 pos, Vec3 motion, float dist, List<Collidable> collidables) {
		while (dist > 0) {
			pp.set(pos).mulAdd(motion, dist);
			for (int j = 0; j < 4; j++) {
				for (int i = 0; i < collidables.size(); i++) {
					collidables.get(i).eject(this, pp);
				}
			}
			pos.set(pp);
			return;
		}
	}
}