package com.ixion.client;

import com.ixion.client.renderer.Shader;

import vecmath.*;

public class Light {
	public float x, y, z;
	public float r, g, b;
	public float ar, ag, ab;
	public float quadraticAttenuation;

	public Light(float x, float y, float z, float r, float g, float b) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.r = r;
		this.g = g;
		this.b = b;

		ar = 0f;
		ag = 0f;
		ab = 0f;
		quadraticAttenuation = 0.2f;
	}

	public void updateUniforms(Mat4 normalMatrix) {
		Shader.lightShader.setUniform("normalMatrix", normalMatrix);

		Shader.lightShader.setUniform("lightSource", x, y, z, 1.0f);
		Shader.lightShader.setUniform("lightDiffuse", r, g, b, 1.0f);
		Shader.lightShader.setUniform("lightAmbient", ar, ag, ab, 1.0f);

		Shader.lightShader.setUniform("quadraticAttenuation", quadraticAttenuation);
	}

	public int getColor() {
		int rr = (int) (r * 255f);
		int gg = (int) (g * 255f);
		int bb = (int) (b * 255f);
		return rr << 16 | gg << 8 | bb;
	}

	public void setColor(int color) {
		int rr = (color & 0xff0000) >> 16;
		int gg = (color & 0xff00) >> 8;
		int bb = (color & 0xff);

		r = rr / 256f;
		g = gg / 256f;
		b = bb / 256f;
	}
}