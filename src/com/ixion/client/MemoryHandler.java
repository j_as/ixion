package com.ixion.client;

import java.nio.*;

public class MemoryHandler {
	private static FloatBuffer fb = createFloatBuffer(4);
	private static ByteBuffer bb = createByteBuffer(4);
	private static DoubleBuffer db = createDoubleBuffer(4);
	private static IntBuffer ib = createIntBuffer(4);
	private static LongBuffer lb = createLongBuffer(4);
	private static ShortBuffer sb = createShortBuffer(4);
	private static CharBuffer cb = createCharBuffer(4);

	public static ByteBuffer getb(byte... src) {
		bb.clear();
		bb.put(src);
		bb.flip();
		return bb;
	}

	public static ShortBuffer gets(short... src) {
		sb.clear();
		sb.put(src);
		sb.flip();
		return sb;
	}

	public static CharBuffer getc(char... src) {
		cb.clear();
		cb.put(src);
		cb.flip();
		return cb;
	}

	public static IntBuffer geti(int... src) {
		ib.clear();
		ib.put(src);
		ib.flip();
		return ib;
	}

	public static LongBuffer getl(long... src) {
		lb.clear();
		lb.put(src);
		lb.flip();
		return lb;
	}

	public static FloatBuffer getf(float... src) {
		fb.clear();
		fb.put(src);
		fb.flip();
		return fb;
	}

	public static DoubleBuffer getd(double... src) {
		db.clear();
		db.put(src);
		db.flip();
		return db;
	}

	public static ByteBuffer createByteBuffer(int size) {
		return ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
	}

	public static ShortBuffer createShortBuffer(int size) {
		return createByteBuffer(size << 1).asShortBuffer();
	}

	public static CharBuffer createCharBuffer(int size) {
		return createByteBuffer(size << 1).asCharBuffer();
	}

	public static IntBuffer createIntBuffer(int size) {
		return createByteBuffer(size << 2).asIntBuffer();
	}

	public static LongBuffer createLongBuffer(int size) {
		return createByteBuffer(size << 3).asLongBuffer();
	}

	public static FloatBuffer createFloatBuffer(int size) {
		return createByteBuffer(size << 2).asFloatBuffer();
	}

	public static DoubleBuffer createDoubleBuffer(int size) {
		return createByteBuffer(size << 3).asDoubleBuffer();
	}

	public static native long getBufferAddress(Buffer buffer);
}