package com.ixion.client;

import static org.lwjgl.opengl.GL11.*;

import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.nio.ByteBuffer;
import java.text.*;
import java.util.Date;

import javax.imageio.ImageIO;

import org.lwjgl.*;
import org.lwjgl.input.*;
import org.lwjgl.openal.*;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

import vecmath.Vec3;

import com.ixion.client.audio.Sound;
//import com.ixion.client.game.*;
import com.ixion.client.menu.*;
import com.ixion.client.renderer.*;

public class Ixion implements Runnable {
	public static final String TITLE = "Ixion";
	public static final String VERSION = "Pre-Alpha";
	public static final Vec3 gravityDir = new Vec3(0, -1f, 0);
	public static final float gravityPower = 9.81f / 60f / 60f;

	private boolean running = true;
	private Screen screen;
	private Renderer renderer = new Renderer();
	private long lastTime = 0;
	private double unprocessedTime = 0;
	
	public Renderer getRenderer() {
		return renderer;
	}

	public void setScreen(Screen nextScreen) {
		if (screen != null) {
			screen.onClose();
			screen = null;
		}

		screen = nextScreen;
		screen.init(this);
	}

	private void init() throws Exception {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int w = gd.getDisplayMode().getWidth() * 8 / 10;
		int h = gd.getDisplayMode().getHeight() * 8 / 10;
		int x = gd.getDisplayMode().getWidth() * 1 / 10;
		int y = gd.getDisplayMode().getHeight() * 1 / 10 / 2;

		Display.setResizable(true);
		Display.setDisplayMode(new DisplayMode(w, h));
		Display.setLocation(x, y);
		Display.setTitle(TITLE);
		Display.setIcon(new ByteBuffer[] { Texture.createByteBuffer(Texture.loadBufferedImage("/textures/display/icon16.png")), Texture.createByteBuffer(Texture.loadBufferedImage("/textures/display/icon32.png")) });
		Display.create(new PixelFormat().withDepthBits(24).withBitsPerPixel(32).withSamples(8));
		Keyboard.create();
		Mouse.create();
		Controllers.create();
		AL.create();

		Shader.create();
		Texture.create();
		Sound.create();
		renderer.init();

		System.out.println(getOS() + (Sys.is64Bit() ? " 64 bit" : " 32 bit"));
		System.out.println(glGetString(GL_VERSION));
		System.out.println("OpenGL " + Sys.getVersion());

		setScreen(new TitleScreen());
		
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	private void tick(double delta) throws Exception {
		while (Keyboard.next()) {
			if (Keyboard.getEventKeyState()) {
				if (Keyboard.isKeyDown(Keyboard.KEY_F9)) {
					toggleFullscreen();
				} else if (Keyboard.isKeyDown(Keyboard.KEY_F7)) {
					saveScreenshot();
				} else {
					screen.keyPressed(Keyboard.getEventKey());
				}
			} else {
				screen.keyReleased(Keyboard.getEventKey());
			}
		}

		while (Mouse.next()) {
			if (Mouse.getEventButtonState()) {
				screen.onClick(Mouse.getEventButton());
			}

			if (!Mouse.isGrabbed() && screen.shouldGrabScreen()) {
				Mouse.setGrabbed(true);
			} else if (!screen.shouldGrabScreen()) {
				Mouse.setGrabbed(false);
			}
		}

		screen.hasFocus = Display.isActive();
		screen.tick(delta);
	}

	private void render() throws Exception {
		int w = Display.getWidth();
		int h = Display.getHeight();

		if (w < 512 || h < 512) {
			if (w < 512) w = 512;
			if (h < 512) h = 512;
			Display.setResizable(false);
			Display.setDisplayMode(new DisplayMode(w, h));
			Display.setResizable(true);
		}
		screen.screenWidth = w;
		screen.screenHeight = h;

		glViewport(0, 0, w, h);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0f, 0f, 0f, 1f);

		screen.render();

		Display.update();
		if (Display.isCloseRequested()) {
			stop();
		}
	}

	private static String getOS() {
		return System.getProperty("os.name");
	}

	public boolean debugMode() {
		return ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	}

	private void toggleFullscreen() throws LWJGLException {
		if (Display.isFullscreen()) {
			GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
			int w = gd.getDisplayMode().getWidth() * 8 / 10;
			int h = gd.getDisplayMode().getHeight() * 8 / 10;
			int x = gd.getDisplayMode().getWidth() * 1 / 10;
			int y = gd.getDisplayMode().getHeight() * 1 / 10 / 2;

			Display.setDisplayMode(new DisplayMode(w, h));
			Display.setLocation(x, y);
		} else {
			Display.setDisplayModeAndFullscreen(Display.getDesktopDisplayMode());
		}
	}

	private void saveScreenshot() throws Exception {
		int x = Display.getX();
		int y = Display.getY();
		int w = Display.getWidth();
		int h = Display.getHeight();
		BufferedImage img = new Robot().createScreenCapture(new Rectangle(x, y, w, h));

		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		Date date = new Date();
		ImageIO.write(img, "png", new File("doc/saved/" + dateFormat.format(date) + ".png"));
	}

	private void stop() {
		running = false;
	}

	public void run() {
		try {
			init();
		} catch (Exception e) {
			System.out.println("Crash when Initialising");
			e.printStackTrace();
		}

		try {
			int ticks = 0;
			int frames = 0;
			long lastFrameTime = System.currentTimeMillis();

			while (running) {
				long nowTime = Sys.getTime();
				long passedTime = nowTime - lastTime;
				lastTime = nowTime;

				if (passedTime < 0 || passedTime >= Sys.getTimerResolution()) {
					passedTime = 0;
				} else {
					unprocessedTime += (passedTime * 60.0) / Sys.getTimerResolution();
				}

				boolean render = false;
				while (unprocessedTime > 1) {
					unprocessedTime -= 1;
					tick(unprocessedTime);
					ticks++;
					render = true;
				}

				if (render) {
					render();
					frames++;
				}

				long nowFrameTime = System.currentTimeMillis();
				if (nowFrameTime - lastFrameTime > 1000) {
					screen.fpsString = frames + " fps, " + ticks + " ticks";
					lastFrameTime += 1000;
					frames = 0;
					ticks = 0;
				}
			}
		} catch (Exception e) {
			System.err.println("Crash during run loop");
			e.printStackTrace();
		}

		this.destroy();
		System.exit(0);
	}

	private void destroy() {
		Shader.destroy();
		Texture.destroy();
		Sound.destroy();

		Display.destroy();
		Keyboard.destroy();
		Mouse.destroy();
		Controllers.destroy();
		AL.destroy();
	}

	public static void main(String[] args) {
		new Thread(new Ixion(), "Game Thread").start();
	}
}