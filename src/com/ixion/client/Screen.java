package com.ixion.client;

import org.lwjgl.input.Keyboard;

import vecmath.*;

import com.ixion.client.renderer.*;

public class Screen {
	protected Ixion ixion;
	protected Renderer renderer;

	public boolean hasFocus = false;
	public int screenWidth, screenHeight;
	public float zoomFactor = 1f;
	public float nearClip = 0.1f;
	public float farClip = 100f;
	public String fpsString = "";

	public Mat4 projectionMatrix = new Mat4();
	public Mat4 modelMatrix = new Mat4();
	public Mat4 viewMatrix = new Mat4();

	public Vec3 right = new Vec3(1, 0, 0);
	public Vec3 up = new Vec3(0, 1, 0);
	public Vec3 forward = new Vec3(0, 0, 1);

	public void init(Ixion ixion) {
		this.ixion = ixion;
		renderer = ixion.getRenderer();
	}

	public void onClose() {
	}

	public boolean shouldGrabScreen() {
		return false;
	}

	public void keyPressed(int eventKey) {
		System.out.println("Key pressed: " + Keyboard.getKeyName(eventKey));
	}

	public void keyReleased(int eventKey) {
		System.out.println("Key released: " + Keyboard.getKeyName(eventKey));
	}

	public void onClick(int mb) {
		System.out.println("Mouse pressed: " + mb);
	}

	public void tick(double delta) {
	}

	public final void render() {
		projectionMatrix.gluPerspective(60f / zoomFactor, getAspectRatio(), nearClip, farClip);
		viewMatrix.identity();
		modelMatrix.identity();

		renderScene();

		projectionMatrix.gluOrthographic(0, screenWidth, 0, screenHeight, -1000f, 1000f);
		viewMatrix.identity();
		modelMatrix.identity();

		renderHud();
	}

	// Render 3d stuff
	public void renderScene() {
	}

	// Render 2d stuff
	public void renderHud() {
		String title = Ixion.TITLE + " " + Ixion.VERSION;
		if (ixion.debugMode()) title = Ixion.TITLE + " " + Ixion.VERSION + " -- [DEBUG MODE]";

		renderer.renderString(title, 7.5f, screenHeight - 25, 0.4f, 0.4f);
		renderer.renderString(fpsString, 7.5f, screenHeight - 45, 0.4f, 0.4f);
	}

	protected void setScreen(Screen screen) {
		ixion.setScreen(screen);
	}

	protected Mat4 createNormalMatrix() {
		Mat4 result = viewMatrix.clone().mul(modelMatrix);
		result.m30 = 0f;
		result.m31 = 0f;
		result.m32 = 0f;
		result.m33 = 1f;
		return result.inverse().transpose();
	}

	protected int hex(int a, int r, int g, int b) {
		if (a > 255) a = 255;
		if (r > 255) r = 255;
		if (g > 255) g = 255;
		if (b > 255) b = 255;

		if (a < 0) a = 0;
		if (r < 0) r = 0;
		if (g < 0) g = 0;
		if (b < 0) b = 0;

		return (a << 24 | b << 16 | g << 8 | r);
	}

	protected int[] argb(int color) {
		int a = (color & 0xff000000) >> 24;
		int r = (color & 0xff0000) >> 16;
		int g = (color & 0xff00) >> 8;
		int b = color & 0xff;

		return new int[] { a, r, g, b };
	}

	public void updateUniforms() {
		Shader.currentShader.setUniform("projectionMatrix", projectionMatrix);
		Shader.currentShader.setUniform("viewMatrix", viewMatrix);
		Shader.currentShader.setUniform("modelMatrix", modelMatrix);

		if (Shader.currentShader == Shader.textureShader || Shader.currentShader == Shader.displayShader) {
			Shader.currentShader.setUniform("tex", 0);
		}
	}

	public void exitGame() {
		System.exit(0);
	}

	private float getAspectRatio() {
		if (screenHeight < 1) return 1;
		return (float) screenWidth / (float) screenHeight;
	}
}