package computer;

import java.util.ArrayList;
import java.util.List;

import computer.command.Command;

public class Mainframe {
	private int MaxIOPorts = 0x3;
	public byte ram = 0x10;

	private List<Command> commands = new ArrayList<Command>();
	private List<MainframeHardware> connectedHardware = new ArrayList<MainframeHardware>();

	public void init() {
		for (MainframeHardware hardware : connectedHardware) {
			hardware.init();
		}
	}

	public void tick(double delta) {
		for (MainframeHardware hardware : connectedHardware) {
			hardware.tick(delta);
		}
	}

	public void addHardware(MainframeHardware hardware, int port) {
		if (port < 0 || port >= MaxIOPorts) return;
		connectedHardware.add(hardware);
	}

	public void addCommand(Command c) {
		commands.add(c);
		c.run(this);
	}

	public VirtualKeyboard getAvailableKeyboard() {
		for (MainframeHardware hardware : connectedHardware) {
			if (hardware instanceof VirtualKeyboard) return (VirtualKeyboard) hardware;
		}
		System.err.println("No available keyboard!");
		return null;
	}

	public VirtualMonitor getAvailableMonitor() {
		for (MainframeHardware hardware : connectedHardware) {
			if (hardware instanceof VirtualMonitor) return (VirtualMonitor) hardware;
		}
		System.err.println("No available monitor!");
		return null;
	}

	public VirtualDrive getAvailableDrive() {
		for (MainframeHardware hardware : connectedHardware) {
			if (hardware instanceof VirtualDrive) return (VirtualDrive) hardware;
		}
		System.err.println("No available drive!");
		return null;
	}

	public VirtualGenerator getAvailableGenerator() {
		for (MainframeHardware hardware : connectedHardware) {
			if (hardware instanceof VirtualGenerator) return (VirtualGenerator) hardware;
		}
		System.err.println("No available generator!");
		return null;
	}
	
	public double getPowerDrainage() {
		double drainage = 0;
		for (MainframeHardware hardware : connectedHardware) {
			drainage += hardware.getPowerDrainage();
		}
		return drainage;
	}

	public void crash() {
		for (MainframeHardware hardware : connectedHardware) {
			hardware.crash();
		}
	}
}