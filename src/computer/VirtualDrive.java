package computer;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class VirtualDrive extends MainframeHardware {
	private String code;
	private ScriptEngineManager factory = new ScriptEngineManager();
	private ScriptEngine engine = factory.getEngineByName("JavaScript");

	public VirtualDrive assemble(String filename) {
		try {
			if (!filename.contains(".fd")) throw new RuntimeException("All floppy discs must be in the correct .fd format!");

			BufferedReader br = new BufferedReader(new FileReader("doc/code/" + filename));
			try {
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				code = sb.toString();
			} finally {
				br.close();
			}
		} catch (Exception e) {
			System.out.println("Could not load JS code!");
			e.printStackTrace();
		}

		return this;
	}

	public void init() {
		/*	try {
				evaluateCode();
				invokeFunction("hello", "jaspreet");
			} catch (Exception e) {
				System.out.println("Could not evaluate code!");
				e.printStackTrace();
			}*/
	}

	public void tick(double delta) {
	}

	public void evaluateCode() throws Exception {
		engine.eval(code);
	}

	public void invokeFunction(String function, Object... args) throws Exception {
		Invocable invoke = (Invocable) engine;
		invoke.invokeFunction(function, args);
	}

	public float getPowerDrainage() {
		return 1;
	}
}