package computer;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import vecmath.Vec3;

import com.ixion.client.Light;
import com.ixion.client.renderer.Tessellator;
import com.ixion.client.renderer.Texture;

public class VirtualMonitor extends MainframeHardware {
	private static class Bitmap {
		public final int w, h;
		public final int[] pixels;

		private String chars = " !\"#$%&'()*+,-./0123456789:;<=>?" + "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_" + "`abcdefghijklmnopqrstuvwxyz{|}~";
		private static final Bitmap[][] font = Bitmap.loadBitmap("/textures/dcpu-font.png", 4, 8);

		public Bitmap(int w, int h) {
			this.w = w;
			this.h = h;
			pixels = new int[w * h];
		}

		public void draw(Bitmap b, int xOffs, int yOffs) {
			for (int y = 0; y < b.h; y++) {
				int yy = y + yOffs;
				if (yy < 0 || yy >= h) continue;

				for (int x = 0; x < b.w; x++) {
					int xx = x + xOffs;
					if (xx < 0 || xx >= w) continue;

					int src = b.pixels[x + y * b.w];
					if (src != 0) pixels[xx + yy * w] = src;
				}
			}
		}

		public void draw(String msg, int x, int y) {
			for (int i = 0; i < msg.length(); i++) {
				int ch = chars.indexOf(msg.charAt(i));
				int xx = ch % 32;
				int yy = ch / 32;
				if (ch >= 0) draw(font[xx][yy], x + i * 4, y);
			}
		}

		public void clear(int col) {
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] = col;
			}
		}

		public void rectangle(int x, int y, int w, int h, int col) {
			fill(x, y, x + w, y + h, col);
		}

		public void fill(int x0, int y0, int x1, int y1, int col) {
			for (int y = y0; y < y1; y++) {
				if (y < 0 || y >= h) continue;
				for (int x = x0; x < x1; x++) {
					if (x < 0 || x >= w) continue;
					pixels[x + y * w] = col;
				}
			}
		}

		public static Bitmap[][] loadBitmap(String name, int sw, int sh) {
			BufferedImage img = null;
			try {
				img = ImageIO.read(Bitmap.class.getResourceAsStream(name));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

			int xs = img.getWidth() / sw;
			int ys = img.getHeight() / sh;

			Bitmap[][] result = new Bitmap[xs][ys];
			for (int x = 0; x < xs; x++) {
				for (int y = 0; y < ys; y++) {
					result[x][y] = new Bitmap(sw, sh);
					img.getRGB(x * sw, y * sh, sw, sh, result[x][y].pixels, 0, sw);
				}
			}
			return result;
		}
	}

	public static final int WIDTH_PIXELS = 128;
	public static final int HEIGHT_PIXELS = 96;
	public static final int MAX_CHARS = 32;
	public static final int MAX_LINES = 9;
	public static final int CHAR_WIDTH = 4;
	public static final int CHAR_HEIGHT = 8;
	public static final int BACKGROUND_COL = 0x0000ff;

	public float displayWidth = WIDTH_PIXELS / 128f;
	public float displayHeight = HEIGHT_PIXELS / 128f;
	public float displayDepth = 0.1f;

	private Bitmap monitorBitmap = new Bitmap(WIDTH_PIXELS, HEIGHT_PIXELS);

	private Vec3 pos;
	private Light light;
	private VirtualKeyboard keyboard;
	private Random random = new Random();

	private boolean cpuCrashed = false;
	private boolean renderTypeFace = false;

	public VirtualMonitor(Vec3 pos) {
		this.pos = pos;

		Vec3 lpos = pos.clone().add(displayWidth / 2f, displayHeight / 2f, -0.5f);
		light = new Light(lpos.x, lpos.y, lpos.z, 0f, 0f, 1f / 3f);
	}

	public void init() {
		keyboard = cpu.getAvailableKeyboard();
		monitorBitmap.rectangle(0, 0, WIDTH_PIXELS, HEIGHT_PIXELS, BACKGROUND_COL);
	}

	public void tick(double delta) {
		renderTypeFace = (System.currentTimeMillis() / 16) / 20 % 2 == 0;
	}

	public void crash() {
		cpuCrashed = true;
		light.r = 1f / 3f;
		light.g = 0f / 3f;
		light.b = 0f / 3f;
	}

	public void render() {
		if (cpuCrashed) {
			renderCrashScreen();
			renderMonitor();
			return;
		}

		monitorBitmap.clear(BACKGROUND_COL);

		for (int j = 0; j < MAX_LINES; j++) {
			for (int i = 0; i < MAX_CHARS; i++) {
				renderLine(i, j);
			}
		}

		if (renderTypeFace) {
			monitorBitmap.rectangle(keyboard.characterPointer * 4, keyboard.linePointer * 8, 4, 8, 0xffffff);
		}
		renderMonitor();
	}

	private void renderCrashScreen() {
		for (int j = 0; j < MAX_LINES; j++) {
			int rr = random.nextInt();
			for (int i = 0; i < MAX_CHARS; i++) {
				int xx = i * CHAR_WIDTH;
				int yy = j * CHAR_HEIGHT;
				monitorBitmap.fill(xx, yy, xx + CHAR_WIDTH, yy + CHAR_HEIGHT, rr);
				if (random.nextInt(100) == 0) monitorBitmap.draw(getRandomChar(), random.nextInt(MAX_CHARS) * CHAR_WIDTH, random.nextInt(MAX_LINES) * CHAR_HEIGHT);
			}
		}
	}

	private String getRandomChar() {
		char randomChar = (char) (48 + random.nextInt(47));
		String s = "";
		s += randomChar;
		return s;
	}

	private void renderLine(int x, int y) {
		monitorBitmap.draw(keyboard.line[x][y].toString(), x * 4, y * 8);
	}

	private void renderMonitor() {
		float zz = 0.015f;

		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);
		t.offset(pos);
		t.color(0x202020, 240);
		t.vertexUV(0f, displayHeight, zz, 1f, 0f);
		t.vertexUV(displayWidth, displayHeight, zz, 0f, 0f);
		t.vertexUV(displayWidth, 0f, zz, 0f, 0.75f);
		t.vertexUV(0f, 0f, zz, 1f, 0.75f);
		t.end();

		Texture.monitorTexture.replacePixels(monitorBitmap.pixels);
		Texture.monitorTexture.enable();

		zz = 0;
		int b = 1;

		t.begin(GL_QUADS);
		t.color(0xffffff);
		t.offset(pos);
		t.vertexUV(0f, displayHeight, zz, (255 + b) / 128f, (127 - b) / 128f);
		t.vertexUV(displayWidth, displayHeight, zz, (127 - b) / 128f, (127 - b) / 128f);
		t.vertexUV(displayWidth, 0f, zz, (127 - b) / 128f, (223f + b) / 128f);
		t.vertexUV(0f, 0f, zz, (255 + b) / 128f, (223f + b) / 128f);
		t.end();

		Texture.monitorTexture.disable();
	}

	public void renderMonitorFrame() {
		float th = 0.02f;

		Tessellator t = Tessellator.instance;
		t.begin(GL_QUADS);
		t.color(0xffffff);
		t.offset(pos);
		t.cube(0f, 0f, displayDepth - th, displayWidth, displayHeight, displayDepth);
		t.cube(-th, 0f, 0f, 0f, displayHeight, displayDepth);
		t.cube(displayWidth, 0f, 0f, displayWidth + th, displayHeight, displayDepth);
		t.cube(-th, -th, 0f, displayWidth + th, 0f, displayDepth);
		t.cube(-th, displayHeight, 0f, displayWidth + th, displayHeight + th, displayDepth);
		t.end();
	}

	public float getPowerDrainage() {
		return 5;
	}

	public Light getLight() {
		return light;
	}
}