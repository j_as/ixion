package computer.command;

import computer.Mainframe;

public class ThreadCommand extends Command {
	private Thread runnable;

	public ThreadCommand(String name, Thread runnable) {
		super(name);
		this.runnable = runnable;
	}

	public void run(Mainframe cpu) {
		runnable.run();
	}
}