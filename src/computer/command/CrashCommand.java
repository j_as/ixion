package computer.command;

import computer.Mainframe;

public class CrashCommand extends Command {
	protected CrashCommand(String name) {
		super(name);
	}

	public void run(Mainframe cpu) {
		cpu.crash();
	}
}
