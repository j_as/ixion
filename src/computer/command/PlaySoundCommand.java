package computer.command;

import com.ixion.client.audio.Sound;
import computer.Mainframe;

public class PlaySoundCommand extends Command {
	private Sound sound;
	
	public PlaySoundCommand(String name, Sound sound) {
		super(name);
		this.sound = sound;
	}

	public void run(Mainframe cpu) {
		sound.play();
	}
}