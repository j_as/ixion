package computer.command;

import computer.Mainframe;
import computer.VirtualKeyboard;

public class ClearCommand extends Command {
	public ClearCommand(String name) {
		super(name);
	}

	public void run(Mainframe cpu) {
		VirtualKeyboard key = cpu.getAvailableKeyboard();
		key.clearLines();
	}
}