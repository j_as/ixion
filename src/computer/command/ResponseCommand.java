package computer.command;

import computer.Mainframe;

public class ResponseCommand extends Command {
	private String response;

	public ResponseCommand(String name, String response) {
		super(name);
		this.response = response;
	}

	public void run(Mainframe cpu) {
		cpu.getAvailableKeyboard().setCurrentLine(response);
	}
}