package computer.command;

import computer.Mainframe;
import computer.VirtualKeyboard;
import computer.VirtualMonitor;

public class ErrorCommand extends Command {
	private String errorString = "Bad command!                                ";

	public ErrorCommand() {
		super(null);
		// Does not need one. Only ran when no other command is found.
	}

	public void run(Mainframe cpu) {
		VirtualKeyboard keyboard = cpu.getAvailableKeyboard();

		keyboard.clearLines();
		for (int i = 0; i < VirtualMonitor.MAX_CHARS; i++) {
			keyboard.line[i][0] = errorString.charAt(i);
		}

		keyboard.characterPointer = 0;
		keyboard.linePointer = 1;
	}
}