package computer.command;

import computer.Mainframe;

public abstract class Command {
	public static final Command playTestSoundCommand = new PlaySoundCommand("play", null);
	public static final Command helloWorldCommand = new ResponseCommand("helloWorld", "hello player!");
	public static final Command clearCommand = new ClearCommand("clear");
	public static final Command halfLifeEasterEggCommand = new ResponseCommand("freeman!", "HL3 confirmed.");
	public static final Command errorMessageCommand = new ErrorCommand();
	public static final Command crashCommand = new CrashCommand("crash");
	/*public static final Command buildModeCommand = new ThreadCommand("buildMode", new Thread(new Runnable() {
		public void run() {
			Ixion.buildMode ^= true;
		}
	}));
*/

	public String name;

	protected Command(String name) {
		this.name = name;
	}

	public abstract void run(Mainframe cpu);

	public static Command has(String commandTitle) {
		if (equals(commandTitle, playTestSoundCommand)) return playTestSoundCommand;
		if (equals(commandTitle, helloWorldCommand)) return helloWorldCommand;
		if (equals(commandTitle, clearCommand)) return clearCommand;
		if (equals(commandTitle, halfLifeEasterEggCommand)) return halfLifeEasterEggCommand;
		if (equals(commandTitle, crashCommand)) return crashCommand;
//		if (equals(commandTitle, buildModeCommand)) return buildModeCommand;
		return errorMessageCommand;
	}

	private static boolean equals(String title, Command c) {
		return title.trim().equalsIgnoreCase(c.name);
	}
}