package computer;

import computer.Mainframe;

public abstract class MainframeHardware {
	public Mainframe cpu;

	public abstract void init();

	public abstract void tick(double delta);

	public MainframeHardware connectTo(Mainframe cpu, int port) {
		this.cpu = cpu;
		cpu.addHardware(this, port);
		return this;
	}

	public float getPowerDrainage() {
		return 1;
	}

	public void crash() {
	}
}