package computer;

import org.lwjgl.input.Keyboard;

import computer.command.Command;

public class VirtualKeyboard extends MainframeHardware {
	private static int INSTANCES = 0;

	public int characterPointer = 0;
	public int linePointer = 0;
	public Character[][] line = new Character[VirtualMonitor.MAX_CHARS][VirtualMonitor.MAX_LINES];

	public VirtualKeyboard() {
		INSTANCES++;
		if (INSTANCES > 1) {
			throw new RuntimeException("Only one keyboard can be connected to the Mainframe!");
		}
	}

	public void init() {
		clearLines();

		setLine("         Mainframe 1.0          ", 0);
		setLine("Boot: success!", 1);
		setLine("RAM: " + cpu.ram + " bytes", 2);
		linePointer = 3;
	}

	public void setLine(String msg, int currentLine) {
		for (int character = 0; character < VirtualMonitor.MAX_CHARS; character++) {
			if (character >= msg.length()) continue;
			line[character][currentLine] = msg.charAt(character);
		}
	}

	public void setCurrentLine(String msg) {
		setLine(msg, linePointer);
	}

	public void clearLines() {
		characterPointer = 0;
		linePointer = 0;

		for (int j = 0; j < VirtualMonitor.MAX_LINES; j++) {
			for (int i = 0; i < VirtualMonitor.MAX_CHARS; i++) {
				line[i][j] = Keyboard.KEY_NONE;
			}
		}
	}

	public void tick(double delta) {
	}

	public void wasPressed(char eventCharacter, int eventKey) {
		if (eventCharacter == Keyboard.CHAR_NONE) return;

		if (linePointer >= VirtualMonitor.MAX_LINES - 1 && characterPointer >= VirtualMonitor.MAX_CHARS - 1) {
			characterPointer--;
			return;
		}

		if (eventKey == Keyboard.KEY_BACK) {
			removeLastCharacter();
			return;
		}
		if (characterPointer == VirtualMonitor.MAX_CHARS) createNewLine();
		if (characterPointer >= 0 && linePointer >= 0 && characterPointer < VirtualMonitor.MAX_CHARS && linePointer < VirtualMonitor.MAX_LINES) line[characterPointer++][linePointer] = eventCharacter;
	}

	private void createNewLine() {
		characterPointer = 0;
		linePointer++;
	}

	private void removeLastCharacter() {
		if (characterPointer == 0 && linePointer == 0) return;

		if (characterPointer < 0 && linePointer > 0) {
			characterPointer = VirtualMonitor.MAX_CHARS - 1;
			linePointer--;
		} else if (characterPointer >= 0 && linePointer >= 0 && characterPointer < VirtualMonitor.MAX_CHARS && linePointer < VirtualMonitor.MAX_LINES) {
			line[characterPointer--][linePointer] = Keyboard.KEY_NONE;
		}
	}

	public void wasReleased(char eventCharacter, int eventKey) {
		if (eventCharacter == Keyboard.CHAR_NONE) return;
		if (eventKey == Keyboard.KEY_RETURN) checkCommandEntered(eventKey);
	}

	public void checkCommandEntered(int eventKey) {
		String commandName = "";

		for (int i = 0; i < VirtualMonitor.MAX_CHARS; i++) {
			commandName += line[i][linePointer];
		}
		Command c = Command.has(commandName);
		cpu.addCommand(c);
	}

	public float getPowerDrainage() {
		return 1;
	}
}